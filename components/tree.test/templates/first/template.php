<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
?>
<div class="poll-section__description">
    <h2 class="poll-section__heading"><?= \Bitrix\Main\Localization\Loc::getMessage('HARD') ?></h2>
    <div class="poll-section__desc">
        <p><?= \Bitrix\Main\Localization\Loc::getMessage('ANSWER') ?></p>
    </div>
</div>
<div class="poll-section__question">
    <div class="question-element">
        <div class="question-element__no">
            <?= \Bitrix\Main\Localization\Loc::getMessage('QUESTION') ?> <span
                    class="no"><?= $arResult['QUESTION_NUMBER'] ?></span>
        </div>
        <form action="/question/" class="question-element__form">
            <h4 class="question-element__heading"><?= $arResult['QUESTION'] ?></h4>
            <div class="question-element__body">
                <? foreach ($arResult['ANSWERS'] as $key => $value) { ?>
                    <div class="control">
                        <label class="control__label">
                            <input type="radio" class="control__input" name="_cure" value="<?= $key ?>">
                            <span class="control__container">
                                    <span class="control__radio"></span>
                                    <span class="control__value"><?= $value ?></span>
                                </span>
                        </label>
                    </div>
                <? } ?>
            </div>
            <div class="question-element__control">
                <button class="button _green"
                        type="submit"><?= \Bitrix\Main\Localization\Loc::getMessage('NEXT') ?></button>
            </div>
        </form>
    </div>
</div>
