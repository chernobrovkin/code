<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
?>

<?
if ($arResult['QUESTION']) {
    require_once dirname(__FILE__) . '/question.php';
} elseif ($arResult['TEXT']) {
    require_once  dirname(__FILE__) . '/final.php';
} else {
    ?>
    <p><?=\Bitrix\Main\Localization\Loc::getMessage('NO_RESULT')?></p>
    <?
}?>
