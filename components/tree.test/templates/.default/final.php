<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$APPLICATION->SetPageProperty('wrapper_class', '_test _test_final');
\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
?>
<div class="questions">
    <div class="question-result">
        <h3 class="question-result__heading">Спасибо за уделенное время!<br> Мы подобрали для вас идеальные варианты
            таблетниц</h3>
        <p class="question-result__text"><?= $arResult['TEXT'] ?></p>
        <div class="question-result__controls">
            <a href="#results" class="question-result__control">
                <svg class="i">
                    <use xlink:href="#arrow-down"/>
                </svg>
            </a>
            <a href="<?= $APPLICATION->GetCurPage() ?>" class="question-result__link">Пройти тест
                заново</a>
        </div>
    </div>
</div>
<div class="section-goods" id="results">
    <? $APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "",
        Array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "ADD_SECTIONS_CHAIN" => "N",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "CACHE_FILTER" => "Y",
            "CACHE_GROUPS" => "N",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_DATE" => "N",
            "DISPLAY_NAME" => "N",
            "DISPLAY_PICTURE" => "N",
            "DISPLAY_PREVIEW_TEXT" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "FIELD_CODE" => array("", ""),
            "FILTER_NAME" => $arParams['FILTER_NAME'],
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => $arParams['IBLOCK_ID_CATALOG'],
            "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE_CATALOG'],
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "INCLUDE_SUBSECTIONS" => "N",
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "20",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Новости",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "PREVIEW_TRUNCATE_LEN" => "",
            "PROPERTY_CODE" => array("BOLD", ""),
            "SET_BROWSER_TITLE" => "N",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "N",
            "SET_META_KEYWORDS" => "N",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SORT_BY1" => "SORT",
            "SORT_BY2" => "ID",
            "SORT_ORDER1" => "ASC",
            "SORT_ORDER2" => "ASC",
            "STRICT_SECTION_CHECK" => "N"
        )
    ); ?>
</div>
<?/*
<script>
    $(function () {
        $('.website-wrapper').removeClass('_packs').addClass('_test_final');
        $('.page__header-text').remove();
        $("a[href*=\\#]:not([href=\\#])").on('click', function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash),
                    headerHeight = $('._js-website-header').height();

                target = target.length ? target : $('[id=' + this.hash.slice(1) + ']');

                if (target.length) {
                    $('._js-website-wrapper').animate({
                        scrollTop: target.offset().top - headerHeight
                    }, 300);

                    return false;
                }
            }
        });
    })
</script>
*/