<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;
use \Bitrix\Iblock;

Loc::loadMessages(__FILE__);

class IbrushTreeTestComponent extends \CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        if (!isset($arParams['FILTER_NAME'])) {
            $arParams['FILTER_NAME'] = 'arrTestResult';
        }

        return $arParams;
    }

    protected function check()
    {
        if (!Loader::includeModule('iblock')) {
            throw new \Exception(Loc::getMessage("NO_IBLOCK_MODULE"));
        }
        if (!$this->arParams['IBLOCK_ID']) {
            throw new \Exception(Loc::getMessage("NEED_IBLOCK"));
        }
    }

    public function executeComponent()
    {
        $this->check();

        $lastAnswer = $this->request->get('_cure') ?: false;

        $db = Iblock\SectionTable::getList([
            'order' => ['SORT' => 'ASC', 'ID' => 'ASC'],
            'filter' => ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $lastAnswer],
            'select' => ['ID', 'NAME', 'DEPTH_LEVEL']
        ]);

        if ($db->getSelectedRowsCount()) {
            while ($row = $db->fetch()) {
                if (!isset($this->arResult['QUESTION_NUMBER'])) {
                    $this->arResult['QUESTION_NUMBER'] = $row['DEPTH_LEVEL'];
                }
                $this->arResult['ANSWERS'][$row['ID']] = $row['NAME'];
            }
            $this->arResult['QUESTION'] = Iblock\ElementTable::getRow([
                'filter' => ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $lastAnswer],
                'select' => ['NAME']
            ])['NAME'];
            $this->arResult['PREV_ANSWER'] = Iblock\SectionTable::getRow([
                'filter' => ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'ID' => $lastAnswer],
                'select' => ['IBLOCK_SECTION_ID']
            ])['IBLOCK_SECTION_ID'];
        } else {
            $resultRow = \CIBlockElement::GetList(
                [],
                ['IBLOCK_ID' => $this->arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'IBLOCK_SECTION_ID' => $lastAnswer],
                false,
                false,
                ['ID', 'NAME', 'PROPERTY_PILLBOXES']
            )->Fetch();
            $this->arResult['TEXT'] = $resultRow['NAME'];
            $GLOBALS[$this->arParams['FILTER_NAME']]['ID'] = $resultRow['PROPERTY_PILLBOXES_VALUE'];
        }

        $this->includeComponentTemplate();
    }
}