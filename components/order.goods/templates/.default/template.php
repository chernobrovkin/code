<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="mm_filter">
    <div class="mm_control">
        <label class="mm_control__label">
            <input type="radio" class="mm_control__input"<?if (!isset($_GET['SECTION_ID'])) {?> checked<?}?> name="SECTION_ID" value="">
            <span class="mm_control__container">
                <span class="mm_control__solid">Показать все</span>
            </span>
        </label>
        <a id="change-section" href="<?= $APPLICATION->GetCurPage() ?>"></a>
    </div>
    <? foreach ($arResult['SECTIONS'] as $arSection) {
        ?>
        <div class="mm_control">
            <label class="mm_control__label">
                <input type="radio" class="mm_control__input"<?if (isset($_GET['SECTION_ID']) && $_GET['SECTION_ID'] == $arSection['ID']) {?> checked<?}?> name="SECTION_ID" value="<?= $arSection['ID'] ?>">
                <span class="mm_control__container">
                    <span class="mm_control__solid"><?= $arSection['NAME'] ?></span>
                </span>
            </label>
            <a id="change-section<?=$arSection['ID']?>" href="<?= $APPLICATION->GetCurPage() ?>?SECTION_ID=<?=$arSection['ID']?>"></a>
        </div>
        <?
    } ?>
</div>

<?
global $arrFavourites;
$arrFavourites = ['ID' => $arResult['ITEM_IDS']];

$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "favourites",
    array(
        "IBLOCK_TYPE" => "catalog",
        "IBLOCK_ID" => "1",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_FIELD2" => "name",
        "ELEMENT_SORT_ORDER2" => "asc",
        "PROPERTY_CODE" => array(
            0 => "OBSHCHEE_TKAN_VERKHA_SOSTAV",
            1 => "OBSHCHEE_SILUET"
        ),
        "META_KEYWORDS" => "-",
        "META_DESCRIPTION" => "-",
        "BROWSER_TITLE" => "-",
        "INCLUDE_SUBSECTIONS" => "Y",
        "BASKET_URL" => "/cart/",
        "ACTION_VARIABLE" => "action",
        "PRODUCT_ID_VARIABLE" => "id",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "PRODUCT_QUANTITY_VARIABLE" => "quantity",
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "FILTER_NAME" => "arrFavourites",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "",
        "CACHE_FILTER" => "",
        "CACHE_GROUPS" => "",
        "SET_TITLE" => "N",
        "SET_STATUS_404" => "N",
        "DISPLAY_COMPARE" => "N",
        "PAGE_ELEMENT_COUNT" => 12,
        "LINE_ELEMENT_COUNT" => "",
        "PRICE_CODE" => $arResult['PRICE_ARRAY'],
        "USE_PRICE_COUNT" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "PRICE_VAT_INCLUDE" => "Y",
        "USE_PRODUCT_QUANTITY" => "Y",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "PARTIAL_PRODUCT_PROPERTIES" => "Y",
        "PRODUCT_PROPERTIES" => [],

        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "Y",
        "PAGER_TITLE" => "Товары",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "catalog",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "",
        "PAGER_SHOW_ALL" => "N",

        "OFFERS_CART_PROPERTIES" => array(
            0 => "RAZMER",
            1 => "ROST",
        ),
        "OFFERS_FIELD_CODE" => [],
        "OFFERS_PROPERTY_CODE" => [
            0 => "RAZMER",
            1 => "ROST",
            2 => "CML2_ARTICLE",
            3 => "CML2_BASE_UNIT",
            4 => "MORE_PHOTO",
            5 => "CML2_MANUFACTURER",
            6 => "CML2_TRAITS",
            7 => "CML2_TAXES",
            8 => "FILES",
            9 => "CML2_ATTRIBUTES",
            10 => "CML2_BAR_CODE"
        ],
        "OFFERS_SORT_FIELD" => "sort",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_FIELD2" => "id",
        "OFFERS_SORT_ORDER2" => "desc",
        "OFFERS_LIMIT" => "0",

        "SECTION_ID" => $_GET['SECTION_ID'] ?? "",
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "DETAIL_URL" => "",
        "SHOW_ALL_WO_SECTION" => "Y",
        "CONVERT_CURRENCY" => "N",
        "CURRENCY_ID" => "RUB",
        "HIDE_NOT_AVAILABLE" => "N",
        "ORDER" => $arResult['ITEM_IDS']

        /*'LABEL_PROP' => $arParams['LABEL_PROP'],
        'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
        'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],

        'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
        'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
        'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
        'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
        'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
        'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
        'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
        'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
        'MESS_BTN_DETAIL' => $arParams['MESS_BTN_DETAIL'],
        'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],

        'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
        "ADD_SECTIONS_CHAIN" => "N",
        'ADD_TO_BASKET_ACTION' => $basketAction,
        'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
        'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare']*/
    )
);