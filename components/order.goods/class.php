<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;

class COrderGoods extends CBitrixComponent
{
    public function executeComponent()
    {
        Loader::includeModule('sale');
        Loader::includeModule('catalog');

        $this->getItems();

        $this->getPriceArray();

        $this->getSections();

        $this->includeComponentTemplate();
    }

    public function getItems()
    {
        $arItems = \Bitrix\Sale\Internals\BasketTable::getList([
            'filter' => [
                'FUSER_ID' => \Bitrix\Sale\Fuser::getId(),
                'DELAY' => 'N',
                '!=ORDER_ID' => false,
                'LID' => \Bitrix\Main\Context::getCurrent()->getSite()
            ],
            'select' => ['PRODUCT_ID'],
            'order' => ['ID' => 'DESC']
        ])->fetchAll();
        if (empty($arItems)) {
            $this->arResult['ITEM_IDS'] = [];
        } else {
            $arProducts = CCatalogSKU::getProductList(array_column($arItems, 'PRODUCT_ID'));
            $this->arResult['ITEM_IDS'] = array_unique(array_column($arProducts, 'ID'));
        }
    }

    public function getPriceArray()
    {
        global $USER;
        $cur_user_id = $USER->GetID();
        $rsUser = CUser::GetByID($cur_user_id);
        $arUser = $rsUser->Fetch();
        $priceType = $arUser['UF_PRICE_VISIBLE'];

        switch ($priceType) {
            case "5": //Розинчные цены
                $price_array = [0 => "Рекомендованная розница"];
                break;
            case "6": //Оптовые цены
                $price_array = [
                    0 => "ОПТ",
                    1 => "Рекомендованная розница",
                ];
                break;
            default: //Нет цены
                $price_array = [0 => "Рекомендованная розница"];
                break;
        }
        $this->arResult['PRICE_ARRAY'] = $price_array;
    }

    public function getSections()
    {
        $sectionsIds = [];
        $db = \Bitrix\Iblock\ElementTable::getList([
            'filter' => ['ID' => $this->arResult['ITEM_IDS']],
            'select' => ['IBLOCK_SECTION_ID', 'PARENT_SECTION' =>'IBLOCK_SECTION.IBLOCK_SECTION_ID'],
            'group' => ['IBLOCK_SECTION_ID']
        ]);

        while ($row = $db->fetch()) {
            $sectionsIds[] = $row['PARENT_SECTION'] ?: $row['IBLOCK_SECTION_ID'];
        }

        $this->arResult['SECTIONS'] = \Bitrix\Iblock\SectionTable::getList([
            'filter' => ['ID' => $sectionsIds],
            'select' => ['ID', 'NAME']
        ])->fetchAll();
    }
}