<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$this->setFrameMode(true);
?>
<div class="control">
    <label class="control__label">
        <input
            type="checkbox"
            class="control__input js-switch-subscribe"<?= ($arResult['IS_SUBSCRIBED'] ? ' checked' : '') ?>
            name="subscribe_control"
        >
        <span class="control__container">
            <span class="control__checkbox"></span>
            <span class="control__value"><?= Loc::getMessage('GET_SUBSCRIBE') ?></span>
        </span>
    </label>
</div>