$(function () {
    $(document).on('change', '.js-switch-subscribe', function () {
        $(this).prop('checked');
        var action = ($(this).prop('checked')) ? 'subscribe' : 'unsubscribe';
        $.get(
            window.location.href,
            {action: action},
            function (data) {
                console.log(data);
            },
            'json'
        );
    });
})