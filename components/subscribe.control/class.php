<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Sender;
use \Bitrix\Main;

class SubscribeControl extends CBitrixComponent
{
    const ACTION_METHODS = [
        'unsubscribe' => 'addUnSubscription',
        'subscribe' => 'addSubscription'
    ];
    const NEED_MODULES = [
        'ibrush.tech',
        'sender'
    ];
    protected $user;
    protected $action;

    private function _checkModules()
    {
        foreach (self::NEED_MODULES as $moduleId) {
            if (!Loader::includeModule($moduleId)) {
                throw new \Exception(Loc::getMessage("SOC_MODULE_NOT_INSTALL"));
            }
        }

        return true;
    }

    public function executeComponent()
    {
        $this->_checkModules();

        $this->user = Main\Engine\CurrentUser::get();

        $this->action = $this->request->get('action');

        if ($this->action && isset(self::ACTION_METHODS[$this->action])) {
            $this->doAction();
        }

        $mailngsIds = array_column(Sender\MailingTable::getList([
            'filter' => ['ACTIVE' => 'Y'],
            'select' => ['ID']
        ])->fetchAll(), 'ID');
        $this->arResult['IS_SUBSCRIBED'] = array_reduce($mailngsIds, [$this, 'checkFullSubscribe']);

        $this->includeComponentTemplate();
    }

    protected function doAction()
    {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $contactId = Sender\ContactTable::addIfNotExist([
            'EMAIL' => $this->user->getEmail()
        ]);
        $mailingsDb = Sender\MailingTable::getList([
            'filter' => ['ACTIVE' => 'Y'],
            'select' => ['ID']
        ]);
        if ($mailingsDb->getSelectedRowsCount()) {
            while ($row = $mailingsDb->fetch()) {
                $res = call_user_func(
                    ['\Bitrix\Sender\MailingSubscriptionTable', self::ACTION_METHODS[$this->action]],
                    [
                        'MAILING_ID' => $row['ID'],
                        'CONTACT_ID' => $contactId
                    ]
                );
                if (!$res) {
                    echo json_encode(['error_messages' => $res->getErrorMessages()]);
                    die();
                }
            }
        } else {
            echo json_encode(['error_messages' => ['NO_MAILNGS']]);
            die();
        }
        die();
    }

    private function checkFullSubscribe($res, $mailingId)
    {
        if ($res === null) {
            $res = true;
        }
        return $res && !(Sender\Subscription::isUnsubscibed($mailingId, $this->user->getEmail()));
    }
}