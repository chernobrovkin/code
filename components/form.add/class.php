<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/*
для создания формы для добавления элемента в инфоблок
*/

class FormAddComponent extends CBitrixComponent
{
    function executeComponent()
    {
        global $USER;
        $userID = $USER->GetID();
        $this->arResult["USER_ID"] = $userID;
        $this->arResult["IBLOCK_ID"] = $this->arParams["IBLOCK_ID"];
        $this->arResult["ACTION"] = $this->GetPath()."/ajax.php";

        $this->arResult["FIELDS"][] = [
            "type" => "text",
            "name" => "NAME",
            "data-name" => "name",
            "required" => "Y",
            "placeholder" => "ФИО",
            "value" => ""
        ];

        $dbProperties = CIBlockProperty::GetList(
            ["sort" => "asc"],
            ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "ACTIVE" => "Y"]
        );
        while($arProperty = $dbProperties->Fetch()) {
            switch ($arProperty["USER_TYPE"]){
                case "UserID":
                    $type = "hidden";
                    break;
                default:
                    $type = "text";
            }
            if ($arProperty["USER_TYPE"] === "Date"){
                $dataName = "date";
            } elseif ($arProperty["CODE"] === "INN"){
                $dataName = "inn";
            } elseif ($arProperty["CODE"] === "SNILS"){
                $dataName = "snils";
            } else {
                $dataName ="text";
            }
            $arField = [
                "type" => $type,
                "name" => "PROPERTY_".$arProperty["CODE"],
                "data-name" => $dataName,
                "required" => $arProperty["IS_REQUIRED"],
                "placeholder" => $arProperty["NAME"],
                "value" => ($arProperty["USER_TYPE"] === "UserID") ? $userID : ""
            ];
            $this->arResult["FIELDS"][] = $arField;
        }

        $this->includeComponentTemplate();
    }
}?>