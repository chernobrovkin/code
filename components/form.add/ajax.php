<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$arFields = [];

foreach ($_POST as $key => $value){
    if (strpos($key, "PROPERTY_") === 0)
        $arFields["PROPERTY_VALUES"][str_replace("PROPERTY_", "", $key)] = $value;
    else
        $arFields[$key] = $value;
}

$el = new CIBlockElement;
if($PRODUCT_ID = $el->Add($arFields))
    echo "Спасибо! Данные сохранены.";
else {
    echo "<pre>"; print_r($arFields); echo "</pre>";
    echo "Error: ".$el->LAST_ERROR;
}