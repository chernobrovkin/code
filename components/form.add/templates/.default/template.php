<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var $arResult array */
?>
<div class="popup__title"><span><span>Введите реквизиты</span></span></div>
<form id="addRequisites" action="<?=$arResult["ACTION"]?>" data-validation="">
    <? foreach ($arResult["FIELDS"] as $arInput) {
        if ($arInput["type"] === "hidden") {
            ?>
            <input
                type="<?= $arInput["type"] ?>"
                name="<?= $arInput["name"] ?>"
                class="input"
                <? if ($arInput["required"] === "Y") { ?>required<? } ?>
                <? if ($arInput["value"]) { ?>value="<?= $arInput["value"] ?>"<? } ?>
            >
            <?
        } else { ?>
            <div class="input-block">
                <input
                    type="<?= $arInput["type"] ?>"
                    name="<?= $arInput["name"] ?>"
                    data-name="<?= $arInput["data-name"]?>"
                    class="input"
                    <? if ($arInput["required"] === "Y") { ?>required<? } ?>
                    <? if ($arInput["value"]) { ?>value="<?= $arInput["value"] ?>"<? } ?>
                >
                <div class="placeholder"><?= $arInput["placeholder"] ?></div>
            </div>
            <?
        }
    } ?>
    <input type="hidden" name="IBLOCK_ID" value="<?=$arResult["IBLOCK_ID"]?>">
    <div class="form__btn form__btn--flex">
        <button type="submit" class="btn">Сохранить</button>
    </div>
</form>