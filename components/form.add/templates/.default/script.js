$(function () {
  var $requisitesForm = $('#addRequisites');
  $requisitesForm.find("[data-name='date']").inputmask("99.99.9999", {
    clearMaskOnLostFocus: true,
    showMaskOnHover: false
  });
  $requisitesForm.find("[data-name='snils']").inputmask("999-999-999 99", {
    clearMaskOnLostFocus: true,
    showMaskOnHover: false
  });
  $(document).on('click','#addRequisites button[type="submit"]', function(e) {
    e.preventDefault();
    if (handleValidity($requisitesForm)) {
      var formData = $requisitesForm.serialize();
      $.post(
        $requisitesForm.attr('action'),
        formData,
        function(msg) {
          $requisitesForm.hide();
          $('#requisites-form span span').html(msg)
        }
      )
    }
  });
});