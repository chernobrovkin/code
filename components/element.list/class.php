<?php

namespace Expocentr\Component;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Expocentr\Helpers\Utils;
use \Bitrix\Main\Context;

\CBitrixComponent::includeComponentClass('expocentr:base');

class ElementListComponent extends BaseComponent
{
    public $requiredModuleList = ['iblock'];
    protected $navParams;
    protected $navigation;
    protected $dbElements;

    public function onPrepareComponentParams($arParams)
    {
        $arParams['IBLOCK_ID'] = $this->getIblockId($arParams['IBLOCK_ID'], $arParams['IBLOCK_TYPE'], SITE_ID, $arParams['CACHE_TIME']);
        $arParams['LANG_ID'] = mb_strtoupper(LANGUAGE_ID);
        if (empty($arParams['SORT_BY'])) {
            $arParams['SORT_BY'] = 'SORT';
        }
        if (empty($arParams['SORT_ORDER'])) {
            $arParams['SORT_ORDER'] = 'ASC';
        }
        if (isset($arParams['SORT_BY2']) && !isset($arParams['SORT_ORDER2'])) {
            $arParams['SORT_ORDER2'] = 'ASC';
        }
        if (!isset($arParams['FIELDS'])) {
            $arParams['FIELDS'] = [];
        }
        if (!isset($arParams['PROPERTIES'])) {
            $arParams['PROPERTIES'] = [];
        }
        if (!empty($arParams['PROPERTIES']) && substr(reset($arParams['PROPERTIES']), 0, 9) === 'PROPERTY_') {
            foreach ($arParams['PROPERTIES'] as &$propertyCode) {
                $propertyCode = substr($propertyCode, 9);
            }
        }
        return $arParams;
    }

    public function executeComponent()
    {
        if (!$this->checkRequiredModuleList()) {
            ShowError('Install required modules');

            return false;
        }

        if (!$this->checkRequiredParameters()) {
            ShowError('Не все обязательные параметры указаны');

            return false;
        }



        $this->navParams = false;

        if (isset($this->arParams["PAGE_SIZE"])) {
            $this->navParams = [
                "nPageSize" => $this->arParams["PAGE_SIZE"]
            ];
        }

        $this->navigation = \CDBResult::GetNavParams($this->navParams);

        if ($this->StartResultCache(false, $this->navigation)) {
            $this->arResult["items"] = $this->getElementList();
            $this->arResult["nav_info"] = $this->getNavInfo();

            if (empty($this->arResult)) {
                return false;
            }

            if ($this->arParams["DUMP_RESULT"] == "Y") {
                echo "<pre>"; dump($this->arResult); echo "</pre>";
            }

            if ($this->arParams["AJAX"] == "Y") {
                return $this->arResult;
            }

            $this->includeComponentTemplate();
        }
    }

    private function checkRequiredParameters()
    {
        if (
            empty($this->arParams['IBLOCK_ID']) ||
            empty($this->arParams['IBLOCK_TYPE'])
        ) {
            return false;
        }

        return true;
    }

    private function getUsefulPropertyValue(array $property)
    {
        switch ($property["PROPERTY_TYPE"]) {
            case "F":
                $value =  Utils::transformValue(['CFile', 'GetPath'], $property["VALUE"]);
                break;
            default:
                $value = Utils::transformValue('htmlspecialcharsBack', $property["VALUE"]);
        }
        return $value;
    }

    private function getElementList()
    {
        $result = [];

        $arSort = [
            $this->arParams['SORT_BY'] => $this->arParams['SORT_ORDER']
        ];

        if (isset($this->arParams['SORT_BY2'])) {
            $arSort[$this->arParams['SORT_BY2']] = $this->arParams['SORT_ORDER2'];
        }

        $arFilter = [
            'IBLOCK_TYPE' => $this->arParams['IBLOCK_TYPE'],
            'IBLOCK_ID'   => $this->arParams['IBLOCK_ID'],
            'ACTIVE'      => 'Y',
        ];

        if (!empty($this->arParams['FILTER'])) {
            $arFilter = array_merge($arFilter, $this->arParams['FILTER']);
        }

        $arSelect = ['ID', 'IBLOCK_ID'];

        $arSelect = array_unique(
            array_merge($arSelect, $this->arParams['FIELDS'])
        );

        $arNavParams = $this->navParams;

        $this->dbElements = \CIBlockElement::GetList(
            $arSort,
            $arFilter,
            false,
            $arNavParams,
            $arSelect
        );

        $this->dbElements->NavStart($this->arParams['PAGE_SIZE']);

        while ($obElement = $this->dbElements->GetNextElement()) {
            $arElement = $obElement->getFields();
            if (isset($arElement['PREVIEW_PICTURE'])) {
                $arElement['PREVIEW_PICTURE'] = \CFile::GetPath($arElement['PREVIEW_PICTURE']);
            }
            if (isset($arElement['DETAIL_PICTURE'])) {
                $arElement['DETAIL_PICTURE'] = \CFile::GetPath($arElement['DETAIL_PICTURE']);
            }
            if (!empty($this->arParams['PROPERTIES'])) {
                $arProperties = $obElement->getProperties();
                if (in_array("*", $this->arParams['PROPERTIES'])) {
                    $propertyCodes = array_keys($arProperties);
                } else {
                    $propertyCodes = $this->arParams['PROPERTIES'];
                }
                foreach ($propertyCodes as $propertyCode) {
                    $arProperty = $arProperties[$propertyCode];
                    $arProperty["CODE"] = strtolower(
                        preg_replace('/_' . $this->arParams['LANG_ID'] . '$/is', '', $arProperty["CODE"])
                    );
                    $arElement[$arProperty["CODE"]] = self::getUsefulPropertyValue($arProperty);
                }
            }
            $result[] = $arElement;
        }

        return $result;
    }

    private function getNavInfo()
    {
        return [
            "cur_page" => $this->dbElements->NavPageNomer,
            "page_size" => $this->dbElements->NavPageSize,
            "page_count" => $this->dbElements->NavPageCount,
            "num" => $this->dbElements->NavNum
        ];
    }
}
