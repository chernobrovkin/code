<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"CACHE_TIME" => array(
			"DEFAULT" => "86400"
		),
		"CURRENCIES" => array(
			"PARENT"   => "BASE",
			"NAME"     => GetMessage("CBR_EXCHABGE_RATES_CURRENCIES"),
			"TYPE"     => "LIST",
			"MULTIPLE" => "Y",
			"VALUES"   => array(
				"USD" => GetMessage("CBR_EXCHABGE_RATES_USD"),
				"EUR" => GetMessage("CBR_EXCHABGE_RATES_EUR"),
			),
			"DEFAULT"  => array("USD", "EUR")
		),
		/*"DATES"      => array(
			"PARENT"   => "BASE",
			"NAME"     => GetMessage("CBR_EXCHABGE_RATES_DATES"),
			"TYPE"     => "STRING",
			"MULTIPLE" => "Y",
			"DEFAULT"  => array(date("d.m.Y"), date("d.m.Y", strtotime("yesterday")))
		),*/
	),
);