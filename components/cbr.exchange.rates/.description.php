<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
	"NAME"        => GetMessage("CBR_EXCHABGE_RATES_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("CBR_EXCHABGE_RATES_COMPONENT_DESCR"),
	"PATH"        => array(
		"ID"    => "service",
		"CHILD" => array(
			"ID"   => "currencies",
			"NAME" => GetMessage("CBR_EXCHABGE_RATES_GROUP_NAME"),
		),
	),
);