<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>
<table class="cbr-rates">
    <thead>
    <tr>
        <td colspan="3">
            <b><a class="cbr-link" href="http://www.cbr.ru/currency_base/daily/" title="Курсы валют ЦБ РФ" target="_blank">Курсы валют ЦБ РФ</a></b>
        </td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            Дата:
        </td>
		<? foreach ($arResult["DATES"] as $rateDate) { ?>
            <td>
				<?= $rateDate ?>
            </td>
		<? } ?>
    </tr>
	<? foreach ($arResult["CURRENCIES"] as $curCode => $arCurrencyInfo) { ?>
        <tr>
            <td>
                <a href="<?= $arCurrencyInfo["DYNAMIC_LINK"] ?>" title="Курс <?= $curCode ?>" target="_blank" class="cbr-link">Курс <?= $curCode ?></a>
            </td>
			<? foreach ($arCurrencyInfo["RATES"] as $rateValue) { ?>
                <td>
					<?= $rateValue ?>
                </td>
			<? } ?>
        </tr>
	<? } ?>
    </tbody>
</table>
