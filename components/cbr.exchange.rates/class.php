<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class CCbrExchangeRatesComponent extends CBitrixComponent
{
	const CBR_USD_CODE = "R01235";
	const CBR_EUR_CODE = "R01239";
	const CBR_SITE_SCRIPT = "http://www.cbr.ru/scripts/XML_dynamic.asp";
	const CBR_SITE_DYNAMIC = "http://www.cbr.ru/currency_base/dynamics/";

	public function executeComponent()
	{
		if ($this->startResultCache()) {
			$this->arResult["PARAMS"] = $this->arParams;
			foreach ($this->arParams["CURRENCIES"] as $currency) {
				$xmlRates = new SimpleXMLElement($this->formRequestUrl($currency), 0, true);
				$arRates = $this->ratesArray($xmlRates);
				$this->arResult["CURRENCIES"][$currency]["RATES"] = $arRates;
				$arDates = array_keys($arRates);
				if (!isset($this->result["DATES"]))
					$this->arResult["DATES"] = $arDates;
				$this->arResult["CURRENCIES"][$currency]["DYNAMIC_LINK"] = $this->formUserLink($currency, $arDates[0], $arDates[1]);
			}
			$this->includeComponentTemplate();
		}
	}

	private function getCbrCurrCode($strCurrency)
	{
		return constant("self::CBR_".$strCurrency."_CODE");
	}

	private function formRequestUrl($currCode)
	{
		$arGetParams = [
			"date_req1" => date("d/m/Y", strtotime("-20 days")),
			"date_req2" => date("d/m/Y", strtotime("+20 days")),
			"VAL_NM_RQ" => $this->getCbrCurrCode($currCode)
		];

		return self::CBR_SITE_SCRIPT."?".http_build_query($arGetParams);
	}

	private function formUserLink($currCode, $from_date, $to_date)
	{
		$arGetParams = [
			"UniDbQuery.Posted" => "True",
			"UniDbQuery.mode" => "1",
			"UniDbQuery.date_req1" => "",
			"UniDbQuery.date_req2" => "",
			"UniDbQuery.VAL_NM_RQ" => $this->getCbrCurrCode($currCode),
			"UniDbQuery.FromDate" => $from_date,
			"UniDbQuery.ToDate" => $to_date
		];
		return self::CBR_SITE_DYNAMIC."?".http_build_query($arGetParams);
	}

	private function xmlNodeToArray(SimpleXMLElement $xmlRecord)
	{
		return [
			"DATE" => $xmlRecord->attributes()["Date"]->__toString(),
			"VALUE" => str_replace(",", ".", $xmlRecord->Value->__toString())
		];
	}

	private function ratesArray(SimpleXMLElement $xmlRoot)
	{
		$arRates = [];
		$xmlRatesCount = $xmlRoot->count();


		$arRatesRecord = $this->xmlNodeToArray($xmlRoot->Record[$xmlRatesCount-2]);
		$arRates[$arRatesRecord["DATE"]] = $arRatesRecord["VALUE"];

		$arRatesRecord = $this->xmlNodeToArray($xmlRoot->Record[$xmlRatesCount-1]);
		$arRates[$arRatesRecord["DATE"]] = $arRatesRecord["VALUE"];

		return $arRates;
	}
}