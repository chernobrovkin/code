BX.addCustomEvent('onAjaxSuccess', function(){
    canvasInit();
});

$(function () {
    canvasInit();
});

function canvasInit() {
    var canvas = document.getElementById('signature-pad');
    var data = document.getElementById('signature-data');

    var signaturePad = new SignaturePad(canvas, {
        penColor: 'rgb(51,85, 170)'
    });

    function resizeCanvas() {
        var ratio = Math.max(window.devicePixelRatio || 1, 1);
        canvas.width = canvas.offsetWidth * ratio;
        canvas.height = canvas.offsetHeight * ratio;
        canvas.getContext("2d").scale(ratio, ratio);
    }

    window.onresize = resizeCanvas;
    resizeCanvas();

    document.getElementById('clear').addEventListener('click', function (e) {
        e.preventDefault();
        signaturePad.clear();
        data.value = '';
    });

    $("#save-png").on('click', function (e) {
        e.preventDefault();

        if (signaturePad.isEmpty()) {
            alert("Пожалуйста поставьте подпись!");
            return;
        }

        document.getElementById('signature-data').value = signaturePad.toDataURL();

        BX.showWait();
        $.post(window.location.href, $('#signature-form').serialize(), function() {
            BX.closeWait();
            $('.js-reload').click();
        });
    });
}