<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$this->setFrameMode(true);
?>
<a href="<?= $APPLICATION->GetCurPage() ?>" class="js-reload"></a>
<h3>Ваша подпись</h3>

<div class="signature">
    <?if ($arResult['SIGNATURE_SRC']) {
        ?>
        <div class="signature__blocks">
            <img class="signature-pad" src="<?= $arResult['SIGNATURE_SRC'] ?>" alt="User signature">
        </div>
        <a href="<?= $APPLICATION->GetCurPage() ?>?edit=Y" class="button signature__button" id="edit-signature">Редактировать</a>
        <?
    } else {?>
        <form id="signature-form" action="<?= $APPLICATION->GetCurPage() ?>" method="POST">
            <div class="signature__blocks">
                <canvas class="signature-pad" id="signature-pad"></canvas>

                <div class="signature__block">
                    <button title="Очистить" class="signature__clear" id="clear">
                        <svg class="i _scoop">
                            <use xlink:href="#scoop" />
                        </svg>
                    </button>
                </div>
            </div>
            <input type="hidden" name="formData" id="signature-data" value="">
            <a href="javascript: void();" class="button signature__button" id="save-png">Сохранить</a>
        </form>
    <? } ?>
</div>