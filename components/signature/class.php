<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Sender;
use \Bitrix\Main;

class Signature extends CBitrixComponent
{
    const ACTION_METHODS = [
        'unsubscribe' => 'addUnSubscription',
        'subscribe' => 'addSubscription'
    ];
    const NEED_MODULES = [
        'ibrush.tech',
        'sender'
    ];
    protected $user;
    protected $action;

    private function _checkModules()
    {
        foreach (self::NEED_MODULES as $moduleId) {
            if (!Loader::includeModule($moduleId)) {
                throw new \Exception(Loc::getMessage("SOC_MODULE_NOT_INSTALL"));
            }
        }

        return true;
    }

    public function executeComponent()
    {
        $this->_checkModules();

        $this->user = Main\Engine\CurrentUser::get();

        if ($this->request->isPost()) {
            $this->saveSignature();
        } else {
            if ($this->request->getQuery('edit') !== 'Y') {
                $row = \Bitrix\Main\UserTable::getRow([
                    'filter' => ['ID' => $this->user->getId()],
                    'select' => ['UF_SIGNATURE']
                ]);
                if ($row['UF_SIGNATURE']) {
                    $this->arResult['SIGNATURE_SRC'] = CFile::GetPath($row['UF_SIGNATURE']);
                }
            }
            $this->includeComponentTemplate();
        }
    }

    protected function saveSignature()
    {
        $GLOBALS['APPLICATION']->RestartBuffer();
        $img = $this->request->getPost('formData');
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $file = $_SERVER['DOCUMENT_ROOT'] . '/upload/signatures/' . uniqid() . '.png';
        $success = file_put_contents($file, $data);
        if ($success) {
            (new \CUser)->Update($this->user->getId(), ['UF_SIGNATURE' => CFile::MakeFileArray($file)]);
            unlink($file);
        }
        die();
    }
}