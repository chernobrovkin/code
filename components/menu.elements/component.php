<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */


$arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);

$arResult["SECTIONS"] = array();
$arResult["ELEMENT_LINKS"] = array();

if($this->StartResultCache()) {
    if (CModule::IncludeModule("iblock")) {
        $arFilter = array(
            "IBLOCK_ID"     => $arParams["IBLOCK_ID"],
            "ACTIVE"        => "Y",
            "IBLOCK_ACTIVE" => "Y"
        );
        $arOrder = array(
            "date_active_from" => "desc",
        );

        $rsElements = CIBlockElement::GetList(
            $arOrder,
            $arFilter,
            false,
            false,
            array(
                "ID",
                "NAME",
                "DETAIL_PAGE_URL",
                "ACTIVE_FROM"
            )
        );
        $rsElements->SetUrlTemplates();
        $currentYear = 9999;
        while ($arElement = $rsElements->GetNext()) {
            $arResult["SECTIONS"][] = array(
                "ID"                => $arElement["ID"],
                "DEPTH_LEVEL"       => 1,
                "~NAME"             => $arElement["~NAME"],
                "~SECTION_PAGE_URL" => $arElement["~DETAIL_PAGE_URL"],
            );
        }
        $this->EndResultCache();
    }
}

$aMenuLinksNew = array();
$menuIndex = 0;
$previousDepthLevel = 1;
foreach($arResult["SECTIONS"] as $arSection)
{
	if ($menuIndex > 0)
		$aMenuLinksNew[$menuIndex - 1][3]["IS_PARENT"] = $arSection["DEPTH_LEVEL"] > $previousDepthLevel;
	$previousDepthLevel = $arSection["DEPTH_LEVEL"];

	$aMenuLinksNew[$menuIndex++] = array(
		htmlspecialcharsbx($arSection["~NAME"]),
		$arSection["~SECTION_PAGE_URL"],
		array(),
		array(
			"FROM_IBLOCK" => true,
			"IS_PARENT" => false,
			"DEPTH_LEVEL" => $arSection["DEPTH_LEVEL"],
		),
	);
}

return $aMenuLinksNew;
?>