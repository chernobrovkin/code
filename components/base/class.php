<?php
namespace Expocentr\Component;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Application;
use \Bitrix\Main\Loader;

/**
 * Class BaseComponent
 */
class BaseComponent extends \CBitrixComponent
{
    public $requiredModuleList = array();

    /**
     * @return bool
     */
    public function checkRequiredModuleList()
    {
        $result = true;

        foreach ($this->requiredModuleList as $moduleName) {
            if (!Loader::includeModule($moduleName)) {
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Get Iblock Id. Can be change to Tools::getIblockId() method
     *
     * @param $code
     * @param $type
     * @param $siteId
     * @param $cacheTime
     * @return bool|null
     */
    public function getIblockId($code, $type, $siteId, $cacheTime = '999999999')
    {
        if (!$code) {
            return false;
        }

        $application = Application::getInstance();
        $obCache = $application->getCache();

        $cacheDir = "/expocentr_component/tool/iblock/{$code}-{$type}-{$siteId}";
        $cacheTime = ($this->arParams["CACHE_TIME"]) ? $this->arParams["CACHE_TIME"] : $cacheTime;

        $id = null;
        if ($obCache->initCache($cacheTime, $cacheDir, $cacheDir)) {
            $id = $obCache->getVars();
        } else if ($obCache->startDataCache()) {
            if (is_numeric($code)) {
                $arFilter["ID"] = $code;
            } else {
                $arFilter["CODE"] = $code;
            }

            if ($type) {
                $arFilter["TYPE"] = $type;
            }

            if ($siteId) {
                $arFilter["SITE_ID"] = $siteId;
            }

            Loader::includeModule('iblock');
            
            $iblock = new \CIBlock();
            $dbIblock = $iblock->GetList(array(), $arFilter, false);
            if ($arIblock = $dbIblock->Fetch()) {
                $id = $arIblock["ID"];
                $obTaggedCache = $application->getTaggedCache();
                $obTaggedCache->startTagCache($cacheDir);
                $obTaggedCache->registerTag("iblock_id_" . $id);
                $obTaggedCache->endTagCache();
                $obCache->endDataCache($id);
            } else {
                $obCache->abortDataCache();
            }
        }

        return $id;
    }
    
    public function set404()
    {
        \Bitrix\Iblock\Component\Tools::process404('', true, true, true, '');
    }
}
