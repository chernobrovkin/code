<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use \Bitrix\Main;
use \Bitrix\Main\Entity;

class UserDocs extends CBitrixComponent
{
    public $arParams;
    public $arResult;
    protected $user;

    private function _checkModules()
    {
        if (!Loader::includeModule('ibrush.tech'))
        {
            throw new \Exception(Loc::getMessage("SOC_MODULE_NOT_INSTALL"));
        }

        return true;
    }

    function executeComponent()
    {
        $this->_checkModules();

        $arFilter = [
            'ACTIVE' => 'Y',
            [
                'LOGIC' => 'OR',
                '=%FULL_NAME' => '%' . @$_REQUEST["q"] . '%',
                '=%FULL_NAME2' => '%' . @$_REQUEST["q"] . '%',
                '=%FULL_NAME3' => '%' . @$_REQUEST["q"] . '%'
            ]
        ];

        $this->arResult["USERS"] = Main\UserTable::getList([
            "select" => ['ID', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'FULL_NAME'],
            "filter" => $arFilter,
            'runtime' => array(
                new Entity\ExpressionField('FULL_NAME', 'CONCAT_WS(\' \', %s, %s, %s)', ['LAST_NAME', 'NAME', 'SECOND_NAME']),
                new Entity\ExpressionField('FULL_NAME2', 'CONCAT_WS(\' \', %s, %s, %s)', ['NAME', 'SECOND_NAME', 'LAST_NAME']),
                new Entity\ExpressionField('FULL_NAME3', 'CONCAT_WS(\' \', %s, %s, %s)', ['NAME', 'LAST_NAME', 'SECOND_NAME']),
            )
        ])->fetchAll();

        foreach($this->arResult["USERS"] as $key => $value){
            $this->arResult["USERS_KEY"][$value['ID']] = $value;
            $this->arResult["USERS_IDS"][$value['ID']] = $value['ID'];
        }

        $arFilter = [
            'IBLOCK_ID' => IBLOCK_ID__EXAMS_APPLICATIONS,
            'PROPERTY_COK' => $this->arParams['COK'],
            'PROPERTY_USER' => $this->arResult["USERS_IDS"]
        ];

        $arNavParams = array(
            "nPageSize" => '6',
            "bShowAll" => 'N',
        );


        $dbEl = \CIBlockElement::GetList(
            array("ID"=>"ASC"),
            $arFilter,
            false,
            $arNavParams,
            array(
                'ID',
                'PROPERTY_USER',
                'PROPERTY_PK.NAME',
            )
        );

        $this->arResult['NAV_COUNT'] = $dbEl->NavRecordCount ? $dbEl->NavRecordCount : '0';

        $this->arResult['NAV_STRING'] = $dbEl->GetPageNavStringEx($navComponentObject, 'Заголовок', 'nark', 'Y');

        while($row = $dbEl->GetNext())
        {
            $this->arResult["ITEMS"][$row['ID']]['USER'] = $this->arResult["USERS_KEY"][$row['PROPERTY_USER_VALUE']]['FULL_NAME'];
            $this->arResult["ITEMS"][$row['ID']]['PK'] = $row['PROPERTY_PK_NAME'];
        }


        $this->includeComponentTemplate();
    }


}