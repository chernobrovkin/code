<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use \Bitrix\Main\Localization\Loc;
?>
<h3>Поиск экзаменов пользователя</h3>
<div class="search-bar">
    <form action="" class="search-bar__form">
        <div class="input">
            <input type="search" name="q" placeholder="Введите имя пользователя" value="<?=@$_REQUEST["q"]?>">
        </div>
        <button class="button" type="submit">Поиск</button>
    </form>
</div>
<div class="divider"></div>
<?
if(!empty($arResult["ITEMS"]) && is_array($arResult["ITEMS"])):
    foreach ($arResult["ITEMS"] as $key => $arUser):
        ?>
        <div class="info-card _done">
            <a href="/personal/users/<?= $key ?>/" class="info-card__link">
                <div class="info-card__content">
                    <header class="info-card__header">
                        <h4 class="info-card__heading"><?= $arUser["USER"] ?></h4>
                    </header>
                    <section class="info-card__section">
                        <h5 class="info-card__section-heading">Профессиональная квалификация:</h5>
                        <p><?= $arUser["PK"] ?></p>
                    </section>
                    <section class="info-card__section">
                        <p>Идентификатор заявки: <?= $key ?></p>
                    </section>
                </div>
                <!--div class="info-card__aside">
                    <div class="info-card__date"></div>
                </div-->
            </a>
        </div>
        <?
    endforeach;

    echo $arResult['NAV_STRING'];
endif;
