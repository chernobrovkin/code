<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\ActionFilter;

Loc::loadMessages(__FILE__);

class Timezone extends CBitrixComponent implements Controllerable
{
    /**
     * @return array
     */
    public function configureActions()
    {
        return [
            'change' => [
                'prefilters' => [
                    new ActionFilter\Authentication()
                ],
                'postfilters' => []
            ]
        ];
    }

	/**
	 * Проверка наличия модулей, требуемых для работы компонента
	 *
	 * @return bool
	 * @throws Exception
	 */
	private function _checkModules()
	{
		if (!Loader::includeModule('iblock') && !Loader::includeModule('ibrush.tech'))
		{
			throw new \Exception(Loc::getMessage("SOC_MODULE_NOT_INSTALL"));
		}

		return true;
	}

	/**
	 * Точка входа в компонент
	 *
	 * @throws Exception
	 */
	public function executeComponent()
	{
		$this->_checkModules();

		$this->arResult['TIMEZONES'] = \CTimeZone::GetZones();
		unset($this->arResult['TIMEZONES']['']);

		$cokId = \Ibrush\Tech\Events\Main::getUserCompany('COK');
		if (!$cokId) {
		    return;
        }
        $this->arResult['COK_TIMEZONE'] = \Ibrush\Tech\Iblock\ElementPropertyTable::getRow(
            array(
                'filter' => array(
                    '=PROPERTY.CODE' => ['TIMEZONE'],
                    '=PROPERTY.IBLOCK_ID' => IBLOCK_ID__COK,
                    'IBLOCK_ELEMENT_ID' => $cokId
                ),
                'select' => array('VALUE', 'CODE' => 'PROPERTY.CODE'),
                'runtime' => array(
                    'PROPERTY' => array(
                        "data_type"   => '\Bitrix\Iblock\PropertyTable',
                        'reference'    => array('=this.IBLOCK_PROPERTY_ID' => 'ref.ID'),
                    )
                )
            )
        )['VALUE'];

		if (empty($this->arResult['COK_TIMEZONE']) || !isset($this->arResult['TIMEZONES'][$this->arResult['COK_TIMEZONE']])) {
		    $this->changeAction(DEFAULT_TIMEZONE, $cokId);
            $this->arResult['COK_TIMEZONE'] = DEFAULT_TIMEZONE;
        }

		$this->IncludeComponentTemplate();
	}

	public function changeAction($timezone, $cokId = false)
    {
        if (!$cokId) {
            $cokId = \Ibrush\Tech\Events\Main::getUserCompany('COK');
        }
        CIBlockElement::SetPropertyValuesEx($cokId, IBLOCK_ID__COK, ['TIMEZONE' => $timezone]);
    }

}