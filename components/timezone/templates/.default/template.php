<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<h4 class="separator">
    <span>Сменить часовой пояс</span>
</h4>

<div class="default-filter _mb-40">
    <form action="<?=$APPLICATION->GetCurPage()?>" class="default-filter__form">
        <div class="cols">
            <div class="col">
                <div class="select">
                    <select>
                        <?foreach ($arResult['TIMEZONES'] as $key => $value) { ?>
                            <option value="<?=$key?>"<?if ($key === $arResult['COK_TIMEZONE']){?> selected<?}?>>
                                <?=$value?>
                            </option>
                        <? } ?>
                    </select>
                </div>
            </div>
        </div>
    </form>
</div>