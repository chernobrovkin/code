$(function () {
    $(document).on('change', '.select select', function () {
        BX.showWait();
        BX.ajax.runComponentAction(
            'ibrush:timezone',
            'change',
            {
                mode: 'class',
                data: {timezone: $(this).val()}
            }
        ).then(function () {
            BX.closeWait();
        });
    });
});