<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$MESS = array(
    "SOC_MODULE_NOT_INSTALL" => "Не установлены все необходимые модули.",
    "SOC_EMPTY_REQUIRED_PARAMS" => "Не указаны обязательные параметры",
);
