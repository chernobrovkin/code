<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$APPLICATION->SetPageProperty('wrapper_class', '_test _packs');
$APPLICATION->AddViewContent('content_after_h1', '<p class="page__header-text">Ответьте на&nbsp;несколько простых вопросов и&nbsp;мы&nbsp;предложим варианты таблетниц, идеально подходящих под&nbsp;ваши требования</p>');

\Bitrix\Main\Localization\Loc::loadMessages(__FILE__);
?>
<div class="questions">
    <div class="question-element">
        <div class="question-element__no">
            <?= \Bitrix\Main\Localization\Loc::getMessage('QUESTION') ?> <span
                    class="no"><?= $arResult['QUESTION_NUMBER'] ?></span>
        </div>
        <form action="<?= $APPLICATION->GetCurPage() ?>" class="question-element__form">
            <h4 class="question-element__heading"><?= $arResult['QUESTION'] ?></h4>
            <div class="question-element__body">
                <div class="question-element__list">
                    <? foreach ($arResult['ANSWERS'] as $key => $value) { ?>
                        <div class="control">
                            <label class="control__label">
                                <input type="radio" class="control__input" name="_cure" value="<?= ($key + 1) ?>">
                                <span class="control__container">
                                    <span class="control__radio"></span>
                                    <span class="control__value"><?=$value?></span>
                                </span>
                            </label>
                        </div>
                    <? } ?>
                </div>
            </div>
            <div class="question-element__control">
                <button class="button _green" type="submit"><?=\Bitrix\Main\Localization\Loc::getMessage('NEXT')?></button>
                <?if ($arResult['QUESTION_NUMBER'] > 1) { ?>
                    <a href="<?=$APPLICATION->GetCurPage() . '?back=Y'?>" class="link-back">
                        <svg class="i">
                            <use xlink:href="#arrow-left"/>
                        </svg>
                        <span class="value"><?=\Bitrix\Main\Localization\Loc::getMessage('PREV')?></span>
                    </a>
                <? } ?>
            </div>
        </form>
    </div>
</div>
<script>
    if (window.location.search !== '')
        history.pushState(null, null, '/question/');
</script>