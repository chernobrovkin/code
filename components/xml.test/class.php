<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Application;
use \Bitrix\Main\Web\Uri;

Loc::loadMessages(__FILE__);

class IbrushXmlTestComponent extends \CBitrixComponent
{

    protected $arAnswers = [];
    protected $xmlTest;

    public function onPrepareComponentParams($arParams)
    {
        if (!isset($arParams['FILTER_NAME'])) {
            $arParams['FILTER_NAME'] = 'arrTestResult';
        }

        return $arParams;
    }

    protected function check()
    {
        if (!$this->arParams['XML_FILE_PATH'] || !file_exists($_SERVER['DOCUMENT_ROOT'] . $this->arParams['XML_FILE_PATH'])) {
            throw new \Exception(Loc::getMessage("FILE_NOT_FOUND"));
        }
    }

    public function executeComponent()
    {
        $this->check();

        $request = Application::getInstance()->getContext()->getRequest();

        if (!isset($_SESSION['TEST'])) {
            $_SESSION['TEST'] = [];
        }

        if (!empty($request->get('_cure'))) {
            $_SESSION['TEST'][] = $_REQUEST['_cure'];
        } elseif ($request->get('back') === 'Y') {
            unset($_SESSION['TEST'][count($_SESSION['TEST']) - 1]);
        } elseif ($request->get('begin') === 'Y') {
            $_SESSION['TEST'] = [];
        }

        $this->arAnswers = $_SESSION['TEST'];

        $xPath = '/test';

        foreach ($this->arAnswers as $answer) {
            $xPath .= '/question/answer[' . $answer . ']';
        }

        $this->xmlTest = new SimpleXMLElement(
            $_SERVER['DOCUMENT_ROOT'] . $this->arParams['XML_FILE_PATH'],
            0,
            true
        );

        $currentPosition = $this->xmlTest->xpath($xPath)[0];

        if ($currentQuestion = $currentPosition->question) {
            $this->arResult['QUESTION_NUMBER'] = count($this->arAnswers) + 1;
            $this->arResult['QUESTION'] = (string)$currentQuestion->attributes()['value'];
            foreach ($currentQuestion->answer as $xmlAnswer) {
                $this->arResult['ANSWERS'][] = (string)$xmlAnswer->attributes()['value'];
            }
        } elseif ($elements = $currentPosition->{'result-list'}->element) {
            foreach ($elements as $element) {
                $this->arResult["ITEMS"][] = (int)$element;
            }
            $this->arResult['TEXT'] = (string)$currentPosition->{'result-text'};
            $GLOBALS[$this->arParams['FILTER_NAME']]['ID'] = $this->arResult["ITEMS"];
        }

        $this->includeComponentTemplate();
    }
}