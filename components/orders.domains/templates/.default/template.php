<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

?>
<table>
    <?php
    foreach ($arResult as $domain => $arItem) { ?>
        <tr>
            <td><?= $domain?></td>
            <td><?= $arItem["COUNT"]?></td>
        </tr>
    <?php
    }
    ?>
</table>
