<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Entity;
use \Bitrix\Sale;
use \Bitrix\Highloadblock;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class COrdersDomainsComponent
 *
 * Класс компонента для выгрузки информации о количестве заказов
 * с группировкой по домену почты пользователя
 */
class COrdersDomainsComponent extends \CBitrixComponent
{
    /**
     * Массив необходимых модулей
     *
     * @var array
     */
    public $requiredModuleList = ['sale', 'highloadblock'];

    /**
     * Класс кеширующего hl-блока
     *
     * @var
     */
    private $hlClass;

    /**
     * timestamp, когда кэширующая таблица последний раз обновлялась
     *
     * @var int
     */
    private $lastUpdated = 0;

    /**
     * Проверка на установленность необходимых модулей
     *
     * @return bool
     */
    public function checkRequiredModuleList()
    {
        $result = true;

        foreach ($this->requiredModuleList as $moduleName) {
            if (!Loader::includeModule($moduleName)) {
                ShowError(Loc::getMessage('NOT_INSTALLED_MODULE') . ' ' . $moduleName);
                $result = false;
            }
        }

        return $result;
    }

    /**
     * Проверка доступа к компоненту
     *
     * @return mixed
     */
    public function checkAccess()
    {
        global $USER;
        return $USER->isAdmin();
    }

    /**
     * Исполнение компонента
     *
     * @return bool
     */
    public function executeComponent()
    {
        if (!$this->checkRequiredModuleList()) {
            return false;
        }

        if (!$this->checkAccess()) {
            ShowError(Loc::getMessage('NO_ACCESS'));
            return false;
        }

        if ($this->StartResultCache()) {

            $arHlBlock = Highloadblock\HighloadBlockTable::getList(
                [
                    'filter' => ['=NAME' => 'OrdersDomains']
                ]
            )->fetch();

            if (!$arHlBlock) {
                $arHlBlock = $this->createHlBlock();
            }

            $this->hlClass = Highloadblock\HighloadBlockTable::compileEntity($arHlBlock)->getDataClass();

            $this->getSavedData();

            $this->refreshData();

            if (empty($this->arResult)) {
                ShowError(Loc::getMessage('NO_DATA'));
                return false;
            }

            $this->includeComponentTemplate();
        }
    }

    /**
     *
     * Забирает данные, уже сохраненные в hl-блоке
     *
     */
    private function getSavedData()
    {
        $dbSavedData = ($this->hlClass)::getList();

        while ($arSavedDomain = $dbSavedData->fetch()) {
            $this->arResult[$arSavedDomain['UF_DOMAIN']] = [
                'ID' => $arSavedDomain['ID'],
                'COUNT' => $arSavedDomain['UF_COUNT']
            ];

            if ($arSavedDomain['UF_DATE_UPDATE']->getTimestamp() > $this->lastUpdated) {
                $this->lastUpdated = $arSavedDomain['UF_DATE_UPDATE']->getTimestamp();
            }
        }
    }

    /**
     *
     * Актуализирует данные из таблицы заказов
     * (выгружаются только новые заказы -
     * сохраненные с момента последнего обновления hl-блока)
     *
     */
    private function refreshData()
    {
        $params = [
            'select' => [
                new Entity\ExpressionField('DOMAIN', 'SUBSTRING_INDEX(%s, \'@\', -1)', ['USER.EMAIL']),
                new Entity\ExpressionField('CNT', 'COUNT(*)')
            ]
        ];

        if ($this->lastUpdated) {
            $params['filter'] = [
                '>=DATE_INSERT' => DateTime::createFromTimestamp($this->lastUpdated)
            ];
        }

        $dbNewData = Sale\Order::getList($params);

        while ($arNewDomain = $dbNewData->fetch()) {
            if (isset($this->arResult[$arNewDomain['DOMAIN']])) {
                $this->arResult[$arNewDomain['DOMAIN']]['COUNT'] += $arNewDomain['CNT'];
                ($this->hlClass)::update(
                    $this->arResult[$arNewDomain['DOMAIN']]['ID'],
                    [
                        'UF_COUNT' => $this->arResult[$arNewDomain['DOMAIN']]['COUNT'],
                        'UF_DATE_UPDATE' => new DateTime()
                    ]
                );
            } else {
                $this->arResult[$arNewDomain['DOMAIN']]['COUNT'] = $arNewDomain['CNT'];
                ($this->hlClass)::add(
                    [
                        'UF_DOMAIN' => $arNewDomain['DOMAIN'],
                        'UF_COUNT' => $arNewDomain['CNT'],
                        'UF_DATE_UPDATE' => new DateTime()
                    ]
                );
            }
        }
    }

    /**
     *
     * Создает hl-блок при первом запуске компонента
     *
     * @return mixed
     */
    private function createHlBlock()
    {
        $result = Highloadblock\HighloadBlockTable::add(
            ['NAME' => 'OrdersDomains', 'TABLE_NAME' => 'b_orders_domains']
        );

        if ($result->isSuccess()) {
            $highBlockID = $result->getId();
            $oUserTypeEntity = new \CUserTypeEntity();
            $arFields = [
                'UF_DOMAIN' => 'string',
                'UF_COUNT' => 'integer',
                'UF_DATE_UPDATE' => 'datetime'
            ];
            foreach ($arFields as $fieldName => $fieldType) {
                $oUserTypeEntity->Add(
                    [
                        'ENTITY_ID' => 'HLBLOCK_' . $highBlockID,
                        'FIELD_NAME' => $fieldName,
                        'USER_TYPE_ID' => $fieldType
                    ]
                );
            }
            $arHlBlock = Highloadblock\HighloadBlockTable::getList(
                [
                    'filter' => ['ID' => $highBlockID]
                ]
            )->fetch();
            return $arHlBlock;
        }
        return false;
    }
}
