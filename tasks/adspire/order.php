<?php

namespace Custom\Handlers;

class Order
{

    public static function OnSaleOrderBeforeSavedHandler(\Bitrix\Main\Event $event)
    {
        $order = $event->getParameter("ENTITY");

        $properties = $order->getPropertyCollection();

        $atmPropertyId = \Bitrix\Sale\Internals\OrderPropsTable::getList(
            [
                'select' => ['ID'],
                'filter' => ['CODE' => 'ATM_COOKIES', 'PERSON_TYPE_ID' => $order->getPersonTypeId()]
            ]
        )->Fetch()['ID'];
        $atmData = $properties->getItemByOrderPropertyId($atmPropertyId);

        if ((string)$atmData->getValue() == '')
        {
            $cookiesCodes = ["atm_marketing", "atm_remarketing", "atm_closer"];
            $context = \Bitrix\Main\Context::getCurrent();
            $request = $context->getRequest();
            $server = $context->getServer();
            $arAdspireData = [];
            foreach ($cookiesCodes as $cookieCode) {
                $arAdspireData[$cookieCode] = $request->getCookieRaw($cookieCode);
            }
            $arAdspireData["adspire_ip"] = ip2long($server->get('REMOTE_ADDR'));
            $atmData->setValue(\Bitrix\Main\Web\Json::encode($arAdspireData, JSON_HEX_TAG|JSON_HEX_AMP));
        }
    }
}