<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("CHK_EVENT", false);
define("CATALOG_IBLOCK_ID", 2);
define("CATALOG_OFFERS_IBLOCK_ID", 3);

use \Bitrix\Sale\Internals\OrderTable;
use \Bitrix\Sale\Basket;
use \Bitrix\Sale\Order;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Web\Json;
use \Bitrix\Main\Loader;

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule('iblock');
Loader::includeModule('catalog');

$f = fopen(realpath(dirname(__FILE__))."/tmp_adspire.xml", "w");
fwrite($f, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
fwrite($f, "<orders date=\"" . date("Y-m-d H:i:s") .  "\">\n");

$dateFrom = new DateTime();
$dateFrom->add("-1 month");

$dbOrders = OrderTable::getList(
    [
        'filter' => ['>=DATE_UPDATE' => $dateFrom, '!=PROPERTY.VALUE' => false, '=PROPERTY.CODE' => 'ATM_COOKIES'],
        'select' => ['ID', 'USER_ID', 'PRICE', 'USER_EMAIL' => 'USER.EMAIL', 'DATE_INSERT', 'STATUS_NAME' => 'STATUS.NAME', 'COMMENTS', 'ADSPIRE_DATA' => 'PROPERTY.VALUE']
    ]
);

while ($arOrder = $dbOrders->fetch()) {
    $arAdspireData = Json::decode($arOrder["ADSPIRE_DATA"]);
    $orderStr = "<order>\n";
    $orderStr .= "<id>" . $arOrder["ID"] . "</id>\n";
    $orderStr .= "<id_user>" . $arOrder["USER_ID"] . "</id_user>\n";
    $orderStr .= "<adspire_ip>" . $arAdspireData["adspire_ip"] . "</adspire_ip>\n";
    $orderStr .= "<type>default</type>\n";
    $orderStr .= "<price>" . $arOrder["PRICE"] . "</price>\n";
    $orderStr .= "<usermail>old</usermail>\n";
    $orderStr .= "<email>" . md5($arOrder["USER_EMAIL"]) . "</email>\n";
    $orderStr .= "<date_order>" . date("Y-m-d H:i:s", $arOrder["DATE_INSERT"]->getTimestamp()) . "</date_order>\n";
    $orderStr .= "<status>" . $arOrder["STATUS_NAME"] . "</status>\n";
    $orderStr .= "<comment>" . $arOrder["COMMENTS"] . "</comment>\n";
    $orderStr .= "<items>\n";

    $basket = Order::load($arOrder["ID"])->getBasket();
    $arBasket = [];
    foreach ($basket as $basketItem) {
        $arBasket[$basketItem->getProductId()] = [
            "pid" => $basketItem->getProductId(),
            "pname" => $basketItem->getField('NAME'),
            "price" => $basketItem->getPrice(),
            "quantity" => $basketItem->getQuantity(),
            "currency" => $basketItem->getField('CURRENCY'),
            "vendor" => "Tom Tailor"
        ];
    }
    $arProducts = CCatalogSku::getProductList(array_keys($arBasket), CATALOG_OFFERS_IBLOCK_ID);
    $arProductsOffers = [];
    foreach ($arProducts as $offerId => $arProduct) {
        $arBasket[$offerId]["group_id"] = $arProduct["ID"];
        $arProductsOffers[$arProduct["ID"]][] = $offerId;
    }
    $dbProducts = CIBlockElement::GetList(
        [],
        ["IBLOCK_ID" => CATALOG_IBLOCK_ID, "ID" => array_keys($arProductsOffers), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"],
        false,
        false,
        ["ID", "IBLOCK_SECTION_ID", "PROPERTY_PATH"]
    );

    while ($arProduct = $dbProducts->Fetch()) {
        foreach ($arProductsOffers[$arProduct["ID"]] as $offerId) {
            $arBasket[$offerId]["cid"] = $arProduct["IBLOCK_SECTION_ID"];
            $arBasket[$offerId]["cname"] = $arProduct["PROPERTY_PATH_VALUE"];
        }
    }

    foreach ($arBasket as $arBasketItem) {
        $orderStr .= "<item>\n";
        foreach ($arBasketItem as $key => $value) {
            $orderStr .= "<$key>$value</$key>\n";
        }
        $orderStr .= "</item>\n";
    }
    $orderStr .= "</items>\n";
    $orderStr .= "<atm_marketing>" . $arAdspireData["atm_marketing"] . "</atm_marketing>\n";
    $orderStr .= "<atm_remarketing>" . $arAdspireData["atm_remarketing"] . "</atm_remarketing>\n";
    $orderStr .= "<atm_closer>" . $arAdspireData["atm_closer"] . "</atm_closer>\n";
    $orderStr .= "</order>\n";
    if (strlen($orderStr) !== 0) {
        fwrite($f, $orderStr);
    }
}
fwrite($f, "</orders>\n");

copy(realpath(dirname(__FILE__))."/tmp_adspire.xml", $_SERVER["DOCUMENT_ROOT"]."/bitrix/catalog_export/adspire_orders.xml");
unlink(realpath(dirname(__FILE__))."/tmp_adspire.xml");
echo "done\n";<?php
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("CHK_EVENT", false);
define("CATALOG_IBLOCK_ID", 2);
define("CATALOG_OFFERS_IBLOCK_ID", 3);

use \Bitrix\Sale\Internals\OrderTable;
use \Bitrix\Sale\Basket;
use \Bitrix\Sale\Order;
use \Bitrix\Main\Type\DateTime;
use \Bitrix\Main\Web\Json;
use \Bitrix\Main\Loader;

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../..");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

Loader::includeModule('iblock');
Loader::includeModule('catalog');

$f = fopen(realpath(dirname(__FILE__))."/tmp_adspire.xml", "w");
fwrite($f, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n");
fwrite($f, "<orders date=\"" . date("Y-m-d H:i:s") .  "\">\n");

$dateFrom = new DateTime();
$dateFrom->add("-1 month");

$dbOrders = OrderTable::getList(
    [
        'filter' => ['>=DATE_UPDATE' => $dateFrom, '!=PROPERTY.VALUE' => false, '=PROPERTY.CODE' => 'ATM_COOKIES'],
        'select' => ['ID', 'USER_ID', 'PRICE', 'USER_EMAIL' => 'USER.EMAIL', 'DATE_INSERT', 'STATUS_NAME' => 'STATUS.NAME', 'COMMENTS', 'ADSPIRE_DATA' => 'PROPERTY.VALUE']
    ]
);

while ($arOrder = $dbOrders->fetch()) {
    $arAdspireData = Json::decode($arOrder["ADSPIRE_DATA"]);
    $orderStr = "<order>\n";
    $orderStr .= "<id>" . $arOrder["ID"] . "</id>\n";
    $orderStr .= "<id_user>" . $arOrder["USER_ID"] . "</id_user>\n";
    $orderStr .= "<adspire_ip>" . $arAdspireData["adspire_ip"] . "</adspire_ip>\n";
    $orderStr .= "<type>default</type>\n";
    $orderStr .= "<price>" . $arOrder["PRICE"] . "</price>\n";
    $orderStr .= "<usermail>old</usermail>\n";
    $orderStr .= "<email>" . md5($arOrder["USER_EMAIL"]) . "</email>\n";
    $orderStr .= "<date_order>" . date("Y-m-d H:i:s", $arOrder["DATE_INSERT"]->getTimestamp()) . "</date_order>\n";
    $orderStr .= "<status>" . $arOrder["STATUS_NAME"] . "</status>\n";
    $orderStr .= "<comment>" . $arOrder["COMMENTS"] . "</comment>\n";
    $orderStr .= "<items>\n";

    $basket = Order::load($arOrder["ID"])->getBasket();
    $arBasket = [];
    foreach ($basket as $basketItem) {
        $arBasket[$basketItem->getProductId()] = [
            "pid" => $basketItem->getProductId(),
            "pname" => $basketItem->getField('NAME'),
            "price" => $basketItem->getPrice(),
            "quantity" => $basketItem->getQuantity(),
            "currency" => $basketItem->getField('CURRENCY'),
            "vendor" => "Tom Tailor"
        ];
    }
    $arProducts = CCatalogSku::getProductList(array_keys($arBasket), CATALOG_OFFERS_IBLOCK_ID);
    $arProductsOffers = [];
    foreach ($arProducts as $offerId => $arProduct) {
        $arBasket[$offerId]["group_id"] = $arProduct["ID"];
        $arProductsOffers[$arProduct["ID"]][] = $offerId;
    }
    $dbProducts = CIBlockElement::GetList(
        [],
        ["IBLOCK_ID" => CATALOG_IBLOCK_ID, "ID" => array_keys($arProductsOffers), "ACTIVE_DATE" => "Y", "ACTIVE" => "Y"],
        false,
        false,
        ["ID", "IBLOCK_SECTION_ID", "PROPERTY_PATH"]
    );

    while ($arProduct = $dbProducts->Fetch()) {
        foreach ($arProductsOffers[$arProduct["ID"]] as $offerId) {
            $arBasket[$offerId]["cid"] = $arProduct["IBLOCK_SECTION_ID"];
            $arBasket[$offerId]["cname"] = $arProduct["PROPERTY_PATH_VALUE"];
        }
    }

    foreach ($arBasket as $arBasketItem) {
        $orderStr .= "<item>\n";
        foreach ($arBasketItem as $key => $value) {
            $orderStr .= "<$key>$value</$key>\n";
        }
        $orderStr .= "</item>\n";
    }
    $orderStr .= "</items>\n";
    $orderStr .= "<atm_marketing>" . $arAdspireData["atm_marketing"] . "</atm_marketing>\n";
    $orderStr .= "<atm_remarketing>" . $arAdspireData["atm_remarketing"] . "</atm_remarketing>\n";
    $orderStr .= "<atm_closer>" . $arAdspireData["atm_closer"] . "</atm_closer>\n";
    $orderStr .= "</order>\n";
    if (strlen($orderStr) !== 0) {
        fwrite($f, $orderStr);
    }
}
fwrite($f, "</orders>\n");

copy(realpath(dirname(__FILE__))."/tmp_adspire.xml", $_SERVER["DOCUMENT_ROOT"]."/bitrix/catalog_export/adspire_orders.xml");
unlink(realpath(dirname(__FILE__))."/tmp_adspire.xml");
echo "done\n";