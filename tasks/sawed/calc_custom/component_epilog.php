<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;

// \\\\\\\\\\\\\\\\\\

$arPaths = array();
$rsPath = CIBlockSection::GetNavChain($arResult["IBLOCK_ID"], $arResult["ID"]);
$rsPath->SetUrlTemplates("", $arParams["SECTION_URL"]);
while($arPath = $rsPath->GetNext())
{
	$arPaths[$arPath['ID']] = $arPath;
	$secIds[] = $arPath['ID'];
}

$resSections = CIBlockSection::GetList(
	array('DEPTH_LEVEL' => 'ASC'),
	array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ID' => $secIds),
	false,
	array('UF_HEADER', 'UF_WORK_URL')
);
while ($arSection = $resSections->Fetch()) {
	$header = $arSection['UF_HEADER'];
	$work_url = $arSection['UF_WORK_URL'];
	$APPLICATION->AddChainItem(
		$arPaths[$arSection['ID']]["NAME"],
		($arSection['UF_WORK_URL'] != '' ? $arSection['UF_WORK_URL'] : $arPaths[$arSection['ID']]["~SECTION_PAGE_URL"] )
	);
}
$arSection['UF_HEADER'] = $header; // сохранено для установки заголовка страницы ниже
$arSection['UF_WORK_URL'] = $work_url; // сохранено для установки атрибута cannonical

if (!empty($arParams["FILTER_NAME"]) && preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	global ${$arParams["FILTER_NAME"]};
}

if (is_array(${$arParams['FILTER_NAME']}) && count(${$arParams['FILTER_NAME']}) > 0) {
	CTrinetSeoFilter::prepareMetaTagsBySeoFilter($arResult["NAME"]);
	CTrinetSeoFilter::setMetaTagsBySeoFilter();

	if (isset($_SERVER['ORIGINAL_REQUEST_URL'])) {
		$APPLICATION->SetPageProperty('canonical', "https://" . SITE_SERVER_NAME . $_SERVER['ORIGINAL_REQUEST_URL']);
	}

}
else {

	// Header
	$pageTitle = \Bitrix\Main\Type\Collection::firstNotEmpty(
		$arSection, 'UF_HEADER'
		,$arResult["IPROPERTY_VALUES"], "SECTION_PAGE_TITLE"
	);
	if ($pageTitle != "")
		$APPLICATION->SetTitle($pageTitle, $arTitleOptions);
	elseif(isset($arResult["NAME"]))
		$APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);

	// Title
	$browserTitle = \Bitrix\Main\Type\Collection::firstNotEmpty(
		$arResult, $arParams["BROWSER_TITLE"]
		,$arResult["IPROPERTY_VALUES"], "SECTION_META_TITLE"
	);
	if (is_array($browserTitle))
		$APPLICATION->SetPageProperty("title", implode(" ", $browserTitle), $arTitleOptions);
	elseif ($browserTitle != "")
		$APPLICATION->SetPageProperty("title", $browserTitle . (( (integer)$_GET['PAGEN_1'] > 1) ? " | Страница " . $_GET['PAGEN_1'] : ""), $arTitleOptions);

	// Meta-keywords
	$metaKeywords = \Bitrix\Main\Type\Collection::firstNotEmpty(
		$arResult, $arParams["META_KEYWORDS"]
		,$arResult["IPROPERTY_VALUES"], "SECTION_META_KEYWORDS"
	);
	if (is_array($metaKeywords))
		$APPLICATION->SetPageProperty("keywords", implode(" ", $metaKeywords), $arTitleOptions);
	elseif ($metaKeywords != "")
		$APPLICATION->SetPageProperty("keywords", $metaKeywords, $arTitleOptions);

	// Meta-description
	$metaDescription = \Bitrix\Main\Type\Collection::firstNotEmpty(
		$arResult, $arParams["META_DESCRIPTION"]
		,$arResult["IPROPERTY_VALUES"], "SECTION_META_DESCRIPTION"
	);
	if (is_array($metaDescription))
		$APPLICATION->SetPageProperty("description", implode(" ", $metaDescription), $arTitleOptions);
	elseif ($metaDescription != "")
		$APPLICATION->SetPageProperty("description", $metaDescription, $arTitleOptions);

	if ($arSection['UF_WORK_URL'] != "") {
		$APPLICATION->SetPageProperty('canonical', "https://" . SITE_SERVER_NAME . $arSection['UF_WORK_URL']);
	}

}
// //////////////////

if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}
?>