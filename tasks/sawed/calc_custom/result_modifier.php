<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Currency\CurrencyTable;

function GetEntityDataClassByTable($HlBlockTableName)
{
    $hlblock = HLBT::getList(["filter" => ["TABLE_NAME" => $HlBlockTableName]])->fetch();
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    return $entity_data_class;
}

$arVariants = [];
$entity_data_class = GetEntityDataClassByTable($arResult["ITEMS"][0]["PROPERTIES"]["SAWED_DEPTH"]['USER_TYPE_SETTINGS']['TABLE_NAME']);
$arData = $entity_data_class::getList(
    array(
        'select' => array('ID', 'UF_XML_ID', 'UF_NAME'),
        'order' => array('UF_SORT' => 'ASC')
    )
)->fetchAll();

foreach ($arData as $arHlEl) {
    $arVariants["SAWED_DEPTH"][$arHlEl["UF_XML_ID"]] = ["NAME" => $arHlEl["UF_NAME"], "ID" => $arHlEl["ID"]];
}

$arResult["VARIANTS"] = $arVariants;

$arResult['CURRENCIES'] = array();
if ($arResult['MODULES']['currency'])
{
    if ($boolConvert)
    {
        $currencyFormat = CCurrencyLang::GetFormatDescription($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);
        $arResult['CURRENCIES'] = array(
            array(
                'CURRENCY' => $arResult['CONVERT_CURRENCY']['CURRENCY_ID'],
                'FORMAT' => array(
                    'FORMAT_STRING' => $currencyFormat['FORMAT_STRING'],
                    'DEC_POINT' => $currencyFormat['DEC_POINT'],
                    'THOUSANDS_SEP' => $currencyFormat['THOUSANDS_SEP'],
                    'DECIMALS' => $currencyFormat['DECIMALS'],
                    'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
                    'HIDE_ZERO' => $currencyFormat['HIDE_ZERO']
                )
            )
        );
        unset($currencyFormat);
    }
    else
    {
        $currencyIterator = CurrencyTable::getList(array(
            'select' => array('CURRENCY')
        ));
        while ($currency = $currencyIterator->fetch())
        {
            $currencyFormat = CCurrencyLang::GetFormatDescription($currency['CURRENCY']);
            $arResult['CURRENCIES'][] = array(
                'CURRENCY' => $currency['CURRENCY'],
                'FORMAT' => array(
                    'FORMAT_STRING' => $currencyFormat['FORMAT_STRING'],
                    'DEC_POINT' => $currencyFormat['DEC_POINT'],
                    'THOUSANDS_SEP' => $currencyFormat['THOUSANDS_SEP'],
                    'DECIMALS' => $currencyFormat['DECIMALS'],
                    'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
                    'HIDE_ZERO' => $currencyFormat['HIDE_ZERO']
                )
            );
        }
        unset($currencyFormat, $currency, $currencyIterator);
    }
}