<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

//define('CATALOG_IBLOCK_ID', 1); //Инфоблок с товарами
//define('CATALOG_IBLOCK_WHOLE_SAWED_SECTION_ID', 398); //Раздел, куда складываем товары
define('VARIANTS_HL_BLOCK_ID', 6); //xайлоад блок с вариантами распила
define('DEPTHS_HL_BLOCK_ID', 8); //xайлоад блок с диапазонами толщин
define('DIMENSIONS_HL_BLOCK_ID', 7); //xайлоад блок с размерами листа
//Цена распила 1 метра в зависимости от толщины
$arTarif = [
    "3-17" => 30,
    "18-30" => 35,
    "31-40" => 40
];

CModule::IncludeModule("iblock");
CModule::IncludeModule("highloadblock");
CModule::IncludeModule("catalog");

//Функция для Декартова произведения
function cartesian($input)
{
    $result = array();

    while (list($key, $values) = each($input)) {
        // If a sub-array is empty, it doesn't affect the cartesian product
        if (empty($values)) {
            continue;
        }

        // Seeding the product array with the values from the first sub-array
        if (empty($result)) {
            foreach ($values as $value) {
                $result[] = array($key => $value);
            }
        } else {
            // Second and subsequent input sub-arrays work like this:
            //   1. In each existing array inside $product, add an item with
            //      key == $key and value == first item in input sub-array
            //   2. Then, for each remaining item in current input sub-array,
            //      add a copy of each existing array inside $product with
            //      key == $key and value == first item of input sub-array

            // Store all items to be added to $product here; adding them
            // inside the foreach will result in an infinite loop
            $append = array();

            foreach ($result as &$product) {
                // Do step 1 above. array_shift is not the most efficient, but
                // it allows us to iterate over the rest of the items with a
                // simple foreach, making the code short and easy to read.
                $product[$key] = array_shift($values);

                // $product is by reference (that why the key we added above
                // will appear in the end result), so make a copy of it here
                $copy = $product;

                // Do step 2 above.
                foreach ($values as $item) {
                    $copy[$key] = $item;
                    $append[] = $copy;
                }

                // Undo the side effecst of array_shift
                array_unshift($values, $product[$key]);
            }

            // Out of the foreach, we can add to $results now
            $result = array_merge($result, $append);
        }
    }

    return $result;
}

//Функцию получения экземпляра класса для работы с xайлоад блоком:
function GetEntityDataClass($HlBlockId)
{
    if (empty($HlBlockId) || $HlBlockId < 1) {
        return false;
    }
    $hlblock = HLBT::getById($HlBlockId)->fetch();
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();
    return $entity_data_class;
}

$arVariants = [];
$arPropsValues = [];
/*$arProps = ["SAWED_DIMENSIONS", "SAWED_DEPTH"];
foreach ($arProps as $propCode) {
    $dbRes = CIBlockProperty::GetPropertyEnum($propCode, array("sort" => "asc"), array("IBLOCK_ID" => CATALOG_IBLOCK_ID));
    while ($arEnum = $dbRes->Fetch()) {
        $arVariants[$propCode][] = $arEnum["ID"];
        $arPropsValues[$arEnum["ID"]] = $arEnum["VALUE"];
    }
}*/


$arProps = [
    "SAWED_DIMENSIONS" => 'DIMENSIONS_HL_BLOCK_ID',
    "SAWED_DEPTH" => 'DEPTHS_HL_BLOCK_ID',
    "SAWED_VARIANT" => 'VARIANTS_HL_BLOCK_ID'
];
foreach ($arProps as $propCode => $constName) {
    $entity_data_class = GetEntityDataClass(constant($constName));
    $arData = $entity_data_class::getList(
        array(
            'select' => array('ID', 'UF_NAME', 'UF_XML_ID')
        )
    )->fetchAll();

    foreach ($arData as $arHlEl) {
        $arVariants[$propCode][] = $arHlEl["UF_XML_ID"];
        $arPropsValues[$propCode][$arHlEl["UF_XML_ID"]] = $arHlEl["UF_NAME"];
    }
}

$arProdVars = cartesian($arVariants);

$el = new CIBlockElement;
$counter = 0;
foreach ($arProdVars as $arProdVariant) {
    $arIblockFields = [
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "IBLOCK_SECTION_ID" => CATALOG_IBLOCK_WHOLE_SAWED_SECTION_ID,
        "PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].'/local/templates/osb_2016/components/bitrix/catalog/.default/bitrix/catalog.section/calc_whole/images/Raspil-fanery-220-220.jpg'),
        "NAME" => "Распил фанеры " . $arPropsValues["SAWED_DIMENSIONS"][$arProdVariant["SAWED_DIMENSIONS"]] . " толщиной " . $arPropsValues["SAWED_DEPTH"][$arProdVariant["SAWED_DEPTH"]] . " на фрагменты " . $arPropsValues["SAWED_VARIANT"][$arProdVariant["SAWED_VARIANT"]],
        "CODE" => "raspil-" . str_replace(" ", "", $arPropsValues["SAWED_DIMENSIONS"][$arProdVariant["SAWED_DIMENSIONS"]]) . "-"
            . $arPropsValues["SAWED_DEPTH"][$arProdVariant["SAWED_DEPTH"]] . "-"
            . str_replace(" ", "", $arPropsValues["SAWED_VARIANT"][$arProdVariant["SAWED_VARIANT"]]),
        "PROPERTY_VALUES" => $arProdVariant
    ];
    $elId = $el->Add($arIblockFields);

    if ($elId)
        echo "New ID: $elId<br>";
    else {
        echo "Error: " . $el->LAST_ERROR;
        die();
    }

    $arProdFields = [
        "ID" => $elId,
        "MEASURE" => 6,
        "QUANTITY" => 999999
    ];
    CCatalogProduct::Add($arProdFields);

    $dimensions = explode("x", $arPropsValues["SAWED_DIMENSIONS"][$arProdVariant["SAWED_DIMENSIONS"]]);
    $sawed = explode("x", $arPropsValues["SAWED_VARIANT"][$arProdVariant["SAWED_VARIANT"]]);
    $length = trim($dimensions[0]) * (trim($sawed[1]) - 1) + trim($dimensions[1]) * (trim($sawed[0]) - 1);
    $price = $arTarif[$arPropsValues["SAWED_DEPTH"][$arProdVariant["SAWED_DEPTH"]]] * $length / 1000;
    $arPriceFields = [
        "PRODUCT_ID" => $elId,
        "CATALOG_GROUP_ID" => 1,
        "CURRENCY" => "RUB",
        "PRICE" => $price
    ];
    CPrice::Add($arPriceFields);
    $counter++;
}
echo "Добавлено $counter элементов";