<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent  $component
 */

use Bitrix\Highloadblock\HighloadBlockTable as HLBT;
use Bitrix\Currency\CurrencyTable;


//Функцию получения экземпляра класса для работы с xайлоад блоком:
function GetEntityDataClassByTable($HlBlockTableName)
{
    $hlblock = HLBT::getList(["filter" => ["TABLE_NAME" => $HlBlockTableName]])->fetch();
    $entity = HLBT::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    return $entity_data_class;
}

$arVariants = [];
$arProps = ["SAWED_DIMENSIONS", "SAWED_DEPTH"];

foreach ($arProps as $propCode) {
    $entity_data_class = GetEntityDataClassByTable($arResult["ITEMS"][0]["PROPERTIES"][$propCode]['USER_TYPE_SETTINGS']['TABLE_NAME']);
    $arData = $entity_data_class::getList(
        array(
            'select' => array('ID', 'UF_XML_ID', 'UF_NAME'),
            'order' => array('UF_SORT' => 'ASC')
        )
    )->fetchAll();

    foreach ($arData as $arHlEl) {
        $arVariants[$propCode][$arHlEl["UF_XML_ID"]] = ["NAME" => $arHlEl["UF_NAME"], "ID" => $arHlEl["ID"]];
    }
}

$entity_data_class = GetEntityDataClassByTable($arResult["ITEMS"][0]["PROPERTIES"]["SAWED_VARIANT"]['USER_TYPE_SETTINGS']['TABLE_NAME']);
$arData = $entity_data_class::getList(
    array(
        'select' => array('ID', 'UF_NAME', 'UF_FILE', 'UF_SAWED_DIMENSIONS', 'UF_XML_ID'),
        'order' => array('UF_SORT' => 'ASC'),
        'filter' => ["=UF_SAWED_DIMENSIONS" => reset($arVariants["SAWED_DIMENSIONS"])["ID"]]
    )
)->fetchAll();

foreach ($arData as $arHlEl) {
    $arVariants["SAWED_VARIANT"][$arHlEl["UF_XML_ID"]] = ["NAME" => $arHlEl["UF_NAME"], "PICTURE_SRC" => CFile::GetPath($arHlEl["UF_FILE"])];
}

$arResult["VARIANTS"] = $arVariants;

$arResult['CURRENCIES'] = array();
if ($arResult['MODULES']['currency']) {
    if ($boolConvert) {
        $currencyFormat = CCurrencyLang::GetFormatDescription($arResult['CONVERT_CURRENCY']['CURRENCY_ID']);
        $arResult['CURRENCIES'] = array(
            array(
                'CURRENCY' => $arResult['CONVERT_CURRENCY']['CURRENCY_ID'],
                'FORMAT'   => array(
                    'FORMAT_STRING'     => $currencyFormat['FORMAT_STRING'],
                    'DEC_POINT'         => $currencyFormat['DEC_POINT'],
                    'THOUSANDS_SEP'     => $currencyFormat['THOUSANDS_SEP'],
                    'DECIMALS'          => $currencyFormat['DECIMALS'],
                    'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
                    'HIDE_ZERO'         => $currencyFormat['HIDE_ZERO']
                )
            )
        );
        unset($currencyFormat);
    } else {
        $currencyIterator = CurrencyTable::getList(
            array(
                'select' => array('CURRENCY')
            )
        );
        while ($currency = $currencyIterator->fetch()) {
            $currencyFormat = CCurrencyLang::GetFormatDescription($currency['CURRENCY']);
            $arResult['CURRENCIES'][] = array(
                'CURRENCY' => $currency['CURRENCY'],
                'FORMAT'   => array(
                    'FORMAT_STRING'     => $currencyFormat['FORMAT_STRING'],
                    'DEC_POINT'         => $currencyFormat['DEC_POINT'],
                    'THOUSANDS_SEP'     => $currencyFormat['THOUSANDS_SEP'],
                    'DECIMALS'          => $currencyFormat['DECIMALS'],
                    'THOUSANDS_VARIANT' => $currencyFormat['THOUSANDS_VARIANT'],
                    'HIDE_ZERO'         => $currencyFormat['HIDE_ZERO']
                )
            );
        }
        unset($currencyFormat, $currency, $currencyIterator);
    }
}