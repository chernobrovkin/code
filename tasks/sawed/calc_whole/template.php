<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

CModule::IncludeModule('currency');
$arItem = $arResult['ITEMS'][0];
$strMainID = $this->GetEditAreaId($arItem['ID']);

$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID . '_pict',
    'SECOND_PICT' => $strMainID . '_secondpict',
    'STICKER_ID' => $strMainID . '_sticker',
    'SECOND_STICKER_ID' => $strMainID . '_secondsticker',
    'QUANTITY' => $strMainID . '_quantity',
    'QUANTITY_DOWN' => $strMainID . '_quant_down',
    'QUANTITY_UP' => $strMainID . '_quant_up',
    'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
    'BUY_LINK' => $strMainID . '_buy_link',
    'BASKET_ACTIONS' => $strMainID . '_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID . '_not_avail',
    'SUBSCRIBE_LINK' => $strMainID . '_subscribe',
    'COMPARE_LINK' => $strMainID . '_compare_link',

    'PRICE' => $strMainID . '_price',
    'DSC_PERC' => $strMainID . '_dsc_perc',
    'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',
    'PROP_DIV' => $strMainID . '_sku_tree',
    'PROP' => $strMainID . '_prop_',
    'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
    'BASKET_PROP_DIV' => $strMainID . '_basket_prop',
);

$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    'TEMPLATE_CLASS' => 'bx_' . $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);
?>
<div class="goods-list calc-input">
    <div id="error-container"></div>
    <form id="calc-input">
        <label>
            Лист форматом
            <select id="dimensions-select" name="dimensions">
                <?
                foreach ($arResult["VARIANTS"]["SAWED_DIMENSIONS"] as $arPropKey => $arPropValue) { ?>
                    <option value="<?= $arPropKey ?>" data-id="<?=$arPropValue["ID"]?>"><?= $arPropValue["NAME"] ?></option>
                    <?
                } ?>
            </select>
            мм
        </label>
        <label>
            Толщина
            <input type="text" name="depth" size="2" id="depth-input">
            мм<br>
            <span class="input-subtext">Допустимый диапазон - 3-40 мм</span>
            <select id="depth-range-select" name="depth-range">
                <option value=""></option>
                <?
                foreach ($arResult["VARIANTS"]["SAWED_DEPTH"] as $arPropKey => $arPropValue) { ?>
                    <option value="<?= $arPropKey ?>"><?= $arPropValue["NAME"] ?></option>
                    <?
                } ?>
            </select>
        </label>
        <label>
            Количество листов
            <div style="display: inline-block; position: relative;">
                <a id="<?= $arItemIDs['QUANTITY_DOWN'] ?>"
                   href="javascript:void(0)"
                   class="bx_bt_button_type_2 bx_small" rel="nofollow">-</a>
                <input type="text" class="bx_col_input"
                       id="<?= $arItemIDs['QUANTITY'] ?>"
                       name="<?= $arParams["PRODUCT_QUANTITY_VARIABLE"] ?>"
                       value="1">
                <a id="<?= $arItemIDs['QUANTITY_UP'] ?>"
                   href="javascript:void(0)"
                   class="bx_bt_button_type_2 bx_small" rel="nofollow">+</a>
                <span id="<?= $arItemIDs['QUANTITY_MEASURE'] ?>">шт. </span>
            </div>
        </label>
        <div class="sawed-variant-header">Выберите вариант распила:</div>
        <div id="sawed-variants-block">
            <div id="sawed-variant">
                <div>
                    <?$i = 0;
                    foreach ($arResult["VARIANTS"]["SAWED_VARIANT"] as $propKey => $arPropValue) {
                        $i++;?>
                        <div class="sawed-variant-container<?=($i == 1)?" selected":""?>" data-value="<?=$propKey?>">
                            <div class="sawed-variant-picture"><img src="<?=$arPropValue["PICTURE_SRC"]?>"></div>
                            <div class="sawed-variant-name"><?=$arPropValue["NAME"]?></div>
                        </div>
                        <?if ($i % 4 === 0 && $i < count($arResult["VARIANTS"]["SAWED_VARIANT"])) echo "</div><div>";
                    }?>
                </div>
            </div>
        </div>
        <input id="sawed-variant-input" type="hidden" name="sawed-variant" value="<?=reset(array_keys($arResult["VARIANTS"]["SAWED_VARIANT"]))?>">
        <input type="hidden" name="iblock_id" value="<?=$arParams["IBLOCK_ID"]?>">
        <input type="hidden" name="hlb_variant_input" value="<?=$arResult["ITEMS"][0]["PROPERTIES"]["SAWED_VARIANT"]['USER_TYPE_SETTINGS']['TABLE_NAME']?>">
    </form>
    <div id="<?= $strMainID ?>" class="calc-result">
        <div class="calc-result-header">Стоимость распила:</div>
        <div id="<?= $arItemIDs['PRICE'] ?>" class="bx_price"><?= $arItem['MIN_PRICE']['PRINT_DISCOUNT_VALUE'] ?></div>
        <div id="<?= $arItemIDs['BASKET_ACTIONS'] ?>"
             class="bx_catalog_item_controls_blocktwo">
            <a id="<?= $arItemIDs['BUY_LINK'] ?>" class="bx_bt_button bx_medium"
               href="javascript:void(0)" rel="nofollow">
                <?php
                if ($arParams['ADD_TO_BASKET_ACTION'] == 'BUY') {
                    echo('' != $arParams['MESS_BTN_BUY'] ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCS_TPL_MESS_BTN_BUY'));
                } else {
                    echo('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET'));
                }
                ?>
            </a>
        </div>
    </div>
</div>
<?
$arJSParams = array(
    'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
//					'PRODUCT_TYPE' => 1,
    'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
    'SHOW_ADD_BASKET_BTN' => false,
    'SHOW_BUY_BTN' => true,
    'SHOW_ABSENT' => true,
    'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
    'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
    'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
    'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
    'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
    'PRODUCT' => array(
        'ID' => $arItem['ID'],
        'NAME' => $arItem['NAME'],
        'PICT' => ["SRC" => $this->GetFolder() . "/images/Raspil-fanery-220-220.jpg"],
        'CAN_BUY' => $arItem["CAN_BUY"],
        'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
        'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
        'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
        'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
        'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
        'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
        'BASIS_PRICE' => $arItem['MIN_PRICE']
    ),
    'BASKET' => array(
        'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
        'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
        'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
        'EMPTY_PROPS' => $emptyProductProperties,
        'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
        'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
    ),
    'VISUAL' => array(
        'ID' => $arItemIDs['ID'],
        'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
        'QUANTITY_ID' => $arItemIDs['QUANTITY'],
        'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
        'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
        'PRICE_ID' => $arItemIDs['PRICE'],
        'BUY_ID' => $arItemIDs['BUY_LINK'],
        'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
        'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
        'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
        'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK']
    ),
    'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
);
?>
<? $strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID); ?>
<script type="text/javascript">
    var <? //echo $strObName; ?>obSawedProduct =
    new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
</script>

<script type="text/javascript">
    BX.message({
        BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
        BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
        ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
        TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
        TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
        TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
        BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
        BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
        BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
        BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
        COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
        COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
        COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
        BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
        SITE_ID: '<? echo SITE_ID; ?>'
    });
</script>