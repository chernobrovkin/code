<?php
namespace Ibrush\Tech\Sender;

use \Bitrix\Main\EventResult;
use \Bitrix\Main\Event;

class EventHandler
{
    /**
     * @param $data
     * @return mixed
     */
    public static function onConnectorListUserFilter($data)
    {
        if (is_array($data)) {
            $data['CONNECTOR'] = '\Ibrush\Tech\Sender\ConnectorUserFilter';
        } elseif ($data instanceof Event) {
            $parameters = ['CONNECTOR' => '\Ibrush\Tech\Sender\ConnectorUserFilter'];
            $data->addResult(new EventResult(EventResult::UNDEFINED, $parameters, 'ibrush.tech'));
        }

        return $data;
    }
}