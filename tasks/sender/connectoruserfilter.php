<?php
namespace Ibrush\Tech\Sender;

use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Loader;

Loc::loadMessages(__FILE__);

if (Loader::includeModule('sender') && Loader::includeModule('iblock'))
{
    class ConnectorUserFilter extends \Bitrix\Sender\Connector
    {
        /**
         * @return string
         */
        public function getName()
        {
            return Loc::getMessage('CONNECTOR_USER_FILTER_NAME');
        }

        /**
         * @return string
         */
        public function getCode()
        {
            return "user_filter";
        }

        /**
         * @return \CDBResult
         * @throws \Bitrix\Main\ArgumentException
         * @throws \Bitrix\Main\ObjectPropertyException
         * @throws \Bitrix\Main\SystemException
         */
        public function getData()
        {
            $groupId = $this->getFieldValue('GROUP_ID', null);
            $regionId = $this->getFieldValue('REGION_ID', null);
            $cityId = $this->getFieldValue('CITY_ID', null);
            $cokId = $this->getFieldValue('COK_ID', null);


            if (!$cityId && $regionId) {
                $regionsDb = \CIBlockElement::GetList(
                    [],
                    ['IBLOCK_ID' => IBLOCK_ID__REGIONS_BITRIX, 'PROPERTY_REGION_BITRIX' => $regionId],
                    false,
                    false,
                    ['PROPERTY_REGION_BITRIX', 'PROPERTY_CITY_BITRIX']
                );
                $cityId = [];
                while ($row = $regionsDb->Fetch()) {
                    $cityId[] = $row['PROPERTY_CITY_BITRIX_VALUE'];
                }
            }

            $filter = ['=ACTIVE' => 'Y'];

            if($groupId) {
                $filter['=\Bitrix\Main\UserGroupTable:USER.GROUP_ID'] = $groupId;
            }

            if ($cityId || $cokId) {
                if (in_array($groupId, [USER_GROUP__SIMPLE_USER, 3, 4]) || !$groupId) {
                    $arAppsFilter = [
                        'IBLOCK_ID' => IBLOCK_ID__EXAMS_APPLICATIONS,
                        'ACTIVE' => 'Y'
                    ];
                    if ($cityId) {
                        $arAppsFilter['PROPERTY_CITY_BITRIX'] = $cityId;
                    }
                    if ($cokId) {
                        $arAppsFilter['PROPERTY_COK'] = $cokId;
                    }
                    $appsDb = \CIBlockElement::GetList(
                        [],
                        $arAppsFilter,
                        false,
                        false,
                        ['PROPERTY_USER']
                    );
                    $userIds = [];
                    while ($row = $appsDb->Fetch()) {
                        $userIds[] = $row['PROPERTY_USER_VALUE'];
                    }
                    $filterSimpleUser = ['=ID' => $userIds];
                }

                if (($groupId && !in_array($groupId, [USER_GROUP__SIMPLE_USER, 3, 4])) || !$groupId) {
                    if ($cityId && !$cokId) {
                        $areaDb = \CIBlockElement::GetList(
                            [],
                            [
                                'IBLOCK_ID' => IBLOCK_ID__COK_PLACES,
                                'ACTIVE' => 'Y',
                                'PROPERTY_CITY_BITRIX' => $cityId
                            ],
                            false,
                            false,
                            ['PROPERTY_COK']
                        );
                        $cokId = [];
                        while ($row = $areaDb->Fetch()) {
                            $cokId[] = $row['PROPERTY_COK_VALUE'];
                        }
                        $cokId = array_unique($cokId);
                    }

                    $filterNarkUser = ['=UF_COK' => $cokId];
                }

                if (!$groupId) {
                    $filter[] = [
                        'LOGIC' => 'OR',
                        $filterSimpleUser,
                        $filterNarkUser
                    ];
                } else {
                    $filter = array_merge(
                        $filter,
                        $filterSimpleUser ?: [],
                        $filterNarkUser ?: []
                    );
                }
            }

            $userDb = \Bitrix\Main\UserTable::getList(array(
                'select' => array('NAME', 'EMAIL', 'USER_ID' => 'ID'),
                'filter' => $filter,
                'order' => array('ID' => 'ASC'),
            ));

            return new \CDBResult($userDb);
        }

        /**
         * @return string
         * @throws \Bitrix\Main\ArgumentException
         * @throws \Bitrix\Main\ObjectPropertyException
         * @throws \Bitrix\Main\SystemException
         */
        public function getForm()
        {
            $groupInput = '<select name="'.$this->getFieldName('GROUP_ID').'">';
            $groupInput .= '<option value="">' . Loc::getMessage('CONNECTOR_USER_FILTER_GROUP_ANY') . '</option>';
            $groupDb = \Bitrix\Main\GroupTable::getList(array(
                'select' => array('ID', 'NAME',),
                'filter' => array('!=ID' => [2, 3/*, 4*/]),
                'order' => array('C_SORT' => 'ASC', 'NAME' => 'ASC')
            ));
            while($group = $groupDb->fetch())
            {
                $inputSelected = ($group['ID'] == $this->getFieldValue('GROUP_ID') ? 'selected' : '');
                $groupInput .= '<option value="'.$group['ID'].'" '.$inputSelected.'>';
                $groupInput .= htmlspecialcharsbx($group['NAME']);
                $groupInput .= '</option>';
            }
            $groupInput .= '</select>';


            $regionsDb = \CIBlockElement::GetList(
                [],
                ['IBLOCK_ID' => IBLOCK_ID__REGIONS_BITRIX],
                false,
                false,
                ['PROPERTY_REGION_BITRIX', 'PROPERTY_CITY_BITRIX']
            );
            $arRegions = [];
            while ($row = $regionsDb->Fetch()) {
                $arRegions[$row['PROPERTY_REGION_BITRIX_VALUE']][] = $row['PROPERTY_CITY_BITRIX_VALUE'];
            }

            if (!empty($arRegions)) {
                $arRegionsIds = array_keys($arRegions);
                $regionInput = '<select name="'.$this->getFieldName('REGION_ID').'">';
                $regionInput .= '<option value="">' . Loc::getMessage('CONNECTOR_USER_FILTER_COK_ANY') . '</option>';
                $arRegionsValues = self::getLocationsById($arRegionsIds);
                asort($arRegionsValues);
                foreach ($arRegionsValues as $id => $name) {
                    $inputSelected = ($id == $this->getFieldValue('REGION_ID') ? 'selected' : '');
                    $regionInput .= '<option value="'.$id.'" '.$inputSelected.'>';
                    $regionInput .= htmlspecialcharsbx($name);
                    $regionInput .= '</option>';
                }
                $regionInput .= '</select>';

                $arCitiesIds = array_reduce(
                    $arRegions,
                    function ($carry, $item) {
                        return array_merge($carry, $item);
                    },
                    []
                );
                $cityInput = '<select name="'.$this->getFieldName('CITY_ID').'">';
                $cityInput .= '<option value="">' . Loc::getMessage('CONNECTOR_USER_FILTER_COK_ANY') . '</option>';
                $arCitiesValues = self::getLocationsById($arCitiesIds);
                asort($arCitiesValues);
                foreach (self::getLocationsById($arCitiesIds) as $id => $name) {
                    $inputSelected = ($id == $this->getFieldValue('CITY_ID') ? 'selected' : '');
                    $cityInput .= '<option value="'.$id.'" '.$inputSelected.'>';
                    $cityInput .= htmlspecialcharsbx($name);
                    $cityInput .= '</option>';
                }
                $cityInput .= '</select>';
            }

            $cokDb = \CIBlockElement::GetList(
                ['NAME' => 'ASC'],
                ['IBLOCK_ID' => IBLOCK_ID__COK, 'ACTIVE' => 'Y'],
                false,
                false,
                ['ID', 'NAME']
            );
            $cokInput = '<select name="'.$this->getFieldName('COK_ID').'">';
            $cokInput .= '<option value="">' . Loc::getMessage('CONNECTOR_USER_FILTER_COK_ANY') . '</option>';
            while ($row = $cokDb->Fetch()) {
                $inputSelected = ($row['ID'] == $this->getFieldValue('COK_ID') ? 'selected' : '');
                $cokInput .= '<option value="'.$row['ID'].'" '.$inputSelected.'>';
                $cokInput .= htmlspecialcharsbx($row['NAME']);
                $cokInput .= '</option>';
            }
            $cokInput .= '</select>';

            return '
				<table>
					<tr>
						<td>'.Loc::getMessage('CONNECTOR_USER_FILTER_GROUP').'</td>
						<td>'.$groupInput.'</td>
					</tr>
					<tr>
						<td>'.Loc::getMessage('CONNECTOR_USER_FILTER_REGION').'</td>
						<td>'.$regionInput.'</td>
					</tr>
					<tr>
						<td>'.Loc::getMessage('CONNECTOR_USER_FILTER_CITY').'</td>
						<td>'.$cityInput.'</td>
					</tr>
					<tr>
						<td>'.Loc::getMessage('CONNECTOR_USER_FILTER_COK').'</td>
						<td>'.$cokInput.'</td>
					</tr>
				</table>
			';
        }

        public static function getLocationsById(array $ids)
        {
            Loader::includeModule('sale');
            $res = \Bitrix\Sale\Location\LocationTable::getList(array(
                'filter' => array('=NAME.LANGUAGE_ID' => LANGUAGE_ID, 'ID' => $ids),
                'select' => array('ID', 'NAME_RU' => 'NAME.NAME')
            ));

            $arData = [];

            while ($arItem = $res->fetch()) {
                $arData[$arItem["ID"]] = $arItem["NAME_RU"];
            }

            return $arData;
        }
    }
}